#!/usr/bin/env bash
################################################################################
#
#   This is supposed to determine (with the DeTerminator script) which OSi
#   it's running under and change some alias's and variables os depending
#
################################################################################
# Another debug
# echo OS is: $OS

# STARTDIR="~/dotfiles"
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

FILE=$SCRIPTPATH/variables.inc && test -f $FILE && source $FILE

case ${U_OS,,} in
    linux)
        if [ $UID -ne 0 ]; then
            alias reboot='sudo reboot'
            #alias update='sudo apt-get upgrade'
        fi
        distro=$(lsb_release -si)
        case $DIST in
            ubuntu)
                source $HOMEDIR/dotfiles/.bashrc
                alias ls='ls -alF --color=auto --group-directories-first'
                alias root='sudo -i'
                alias su='sudo -i'
                # Unalias
                # unalias nano
                ;;

            rhel|centos|fedora)
                alias update="sudo yum update"
                alias upgrade="sudo yum safe-upgrade"
                alias install="sudo yum install"
                alias uninstall="sudo yum remove"
                alias updatey='yum -y update'
                #function install() {
                #    yum install "$@" ;
                #}
                alias ls='ls -alF'
                # Unalias
                # unalias nano
                ;;

            peppermint) # echo peppermint/debian
                source $HOMEDIR/dotfiles/.bashrc
                alias ll='ls -alF --group-directories-first'
                unalias nano
                ;;

            arch)
                alias update='install'
                alias upgrade='sudo pacman -Syu'
                alias install='sudo pacman -Sy'
                alias uninstall='sudo pacman -Rs'
                ;;

            *)
                echo "Sorry, Linux distribution '$DIST' is not supported"
                #exit 1
                ;;
        esac
        ;;
    freebsd|FreeBSD) echo=FreeBSD
        # Unalias
        unlias ls
        alias l='ls -alFGh -D "%F %H:%M"'
        # Reinstall
        alias reinstall="make deinstall && make reinstall clean"
        alias install="make install clean"
        # Version function
        function vv() {
            portmaster -L --index-only| egrep "(ew|ort) version|total install"
        }
        ;;
    windows_nt|cygwin|windows) echo=windows
        export SHELLOPTS
        set -o igncr
        . $HOME/dotfiles/.cygwin.d/.cygwin_alias
        . $HOME/dotfiles/.bashrc.d/.bash_alias_common
        #temp removed. ~/.inputrc # for cygwin
        ;;

    darwin) echo=Mac
        alias ls="ls -h"
        ;;

    sunos|Solaris) #echo=Solaris
        FILE1=~/.dotfiles_old/.bashrc ; [ -f $FILE1 ] && . $FILE1
        FILE2=~/.dotfiles_old/.profle ; [ -f $FILE2 ] && . $FILE2
        alias update="sudo pkg update"
        alias install="pkgadd -d" $1
        # Unalias
        unalias nano
        ;;

    aix) echo=AIX
        ;;

    GNU/Linux)
        # Ubuntu/Linux on windows 10
        echo "Ubuntu on Windows"
        ;;

    *) echo "Sorry, OS '$U_OS' is not supported"
        # Not discovered OS's goes here
        # unalias nano
        ;;
esac

# Check to see if the OS var was loaded
echo -e "I've concluded that you are running \e[31m$U_OS\e[0m with \e[31m$DIST\e[0m"

# Check if variable is set/has a value
# if [[ ! -z $DIST ]]; then echo $DIST; fi

# Shows IP - Not working as it should right now
# echo -e "IP: \e[31m$LocalIP\e[0m"

