#!/bin/bash

# Default directory
DEFAULT_DIR="/media/Downloads/complete"

# Directory specified by user (or default directory if not specified)
DIR=${1:-$DEFAULT_DIR}

# File containing list of file types to delete
FILE_TYPES="list_of_trash_files.inc"

# Check if directory exists
if [ ! -d "$DIR" ]; then
    echo "Directory $DIR does not exist."
    exit 1
fi

# Check if file types file exists
if [ ! -f "$FILE_TYPES" ]; then
    echo "File $FILE_TYPES does not exist."
    exit 1
fi

# Read file types from file and delete those file types
while IFS= read -r line
do
    find "$DIR" -name "*.$line" -type f -delete
done < "$FILE_TYPES"

# Find and remove empty directories
find "$DIR" -type d -empty -delete

echo "Cleanup completed."