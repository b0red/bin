#!/bin/bash

# Define the directory to search and the destination directory
SEARCH_DIR="/media/Movies/Filmer_Temp"
DEST_DIR="/media/Movies/DeleteMovies"
LOGG_FILE="/media/Movies/DeleteMovies/MovieDeletsLog.txt"

# Create the destination directory if it doesn't exist
mkdir -p "$DEST_DIR"

# Find all directories in the search directory
find "$SEARCH_DIR" -maxdepth 1  -type d | while read -r dir; do
  # Check if the directory contains any movie files
  if ! find "$dir" -type f \( -iname "*.mov" -o -iname "*.avi" -o -iname "*.mkv" -o -iname "*.vob" -o -iname "*.wmv" -o -iname "*m4v" -o -iname "*mp4" -o -iname "*flv" -o -iname "*f4v" \) | grep -q .; then
    # Move the directory to the destination directory if no movie files are found
    mv "$dir" "$DEST_DIR"
    echo "$dir" >> "$LOGG_FILE"
  fi
done
