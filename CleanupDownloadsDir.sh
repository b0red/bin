#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear

[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && debug=0 || debug=1    # check if cron or interactivly
start=$(date +%s)                                               # Start the elapsed timer

#######################################################################################################
##
##	Info: Script for cleaning up empty folders and listed files in delete_files.inc 
##  in folder: /media/downloads (this can be changed)
##
##	Set it to run everyh xth hour in crontab
##
##      Requirements
##      This script needs a file called [list_of_trash_files.inc] containing
##      files and filetypes that it will clean when run. This file must be in the 
##      same directory as this file, else change it. 
##      
##      Also, You need [email_variables.inc], containing the emailadress/pushover to send
##      stuff to.
##
##      Dependencies:
##      a file with email/pushover credentials, or you can hardcode them into this script
##      a file called delete_files.inc containei filetype to search and delete
##
#######################################################################################################

###     Settings - Do NOT change!
#
# Check for and use this file if it exists (it should)
# SCRIPT=$(readlink -f "$1")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
pushd $(dirname "$0") > /dev/null || exit
SCRIPTPATH=$(pwd -P)
popd > /dev/null ||exit

file=$SCRIPTPATH/email_variables.inc && test -f "$file" && source "$file"       # email/pushover
file_2=$SCRIPTPATH/ColorCodes.inc && test -f "$file_2" && source "$file_2"      # pretty colors on screen

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Change stuff between these lines

#debug=1                                            # Just shows my debug info after a run
trace_debug=0                                       # Does a bash step debug when enabled at program run

pushit=0                                            # Sends a pushover notification
deleteit=1                                          # If you want to delete files & folders

now=$(date '+%Y-%m-%d %H:%M')                       # get date
node=$(uname -n)                                    # get node/machine name
basedir=/media/Downloads/complete                   # Startfolder
startdir=${1:-$basedir}                             # Default startdir or users choice

pushtitle="Message from ${0##*/}!"                  # Title of the messages
pushtitle=$(echo "$pushtitle" | cut -f 1 -d '.')    # Title of the messages

cleanfiles="${SCRIPTPATH}/list_of_trash_files.inc"     # List of files to delete
empty_status=~/tmp/empty_dirs.txt

#       Dont change beyond this line
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

###     Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

###     Send notification from pushover
#
function push_message() {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "message=$status" https://api.pushover.net/1/messages.json
    }

###     Send Gotify notifications
#
function notify_gotify() {
    TITLE="${pushtitle}"
    MESSAGE="${status}:!"
    PRIORITY=5
    URL="${GOTIFY_URL}/message?token=${GOTIFY_TOKEN}"
    curl -s -S --data '{"message": "'"${MESSAGE}"'", "title": "'"${TITLE}"'", "priority":'"${PRIORITY}"', "extras": {"client::display": {"contentType": "text/markdown"}}}' -H 'Content-Type: application/json' "$URL"
}

###     Time elapsed counter
#
function secs_to_human() {
    if [[ -z ${1} || ${1} -lt 60 ]] ;then
        min=0 ; secs="${1}"
        else
            time_mins=$(echo "scale=2; ${1}/60" | bc)
            min=$(echo ${time_mins} | cut -d'.' -f1)
            secs="0.$(echo ${time_mins} | cut -d'.' -f2)"
            secs=$(echo ${secs}*60|bc|awk '{print int($1+0.5)}')
    fi
    elapse_status="Time Elapsed: ${min} minutes and ${secs} seconds. "
}

###     Check for network connection
#
function network_check() {
    if echo -n >/dev/tcp/8.8.8.8/53; then
        net_status="Network available. "; network=1; text_color="$GREEN"
        else
            net_status="Offline. ";  network=0; text_color="$RED"
    fi
}

###     Count number of files to search for
#
function file_types(){
    delete_types=$(wc -l < ${cleanfiles})
}

###     Empty folders
#
function count_empty_folders() {
    if [ ! -f ${empty_status} ]; then touch ${empty_status} ; fi
    #local empty_folder_count=0
    for folder in ${startdir} find -type d -empty ; do
        ((empty_folder_count++))
    done
    echo "The number of empty folders is: ${empty_folder_count}" > ${empty_status}
}

[[ $deleteit -eq 1 ]] && del_status="yes"; text_color="$RED"|| del_status="no"; text_color="$GREEN"

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Do stuff & call functions
#
network_check       
count_empty_folders
notify_gotify
file_types

###     Check for directory
#
if [ ! -d ${startdir} ]; then
    status="Couldn't find folder: ${startdir}. Quitting!"
    push_message ${status}
    echo $status 1
    exit 0
 elif [ ! -f $cleanfiles ]; then
        status="Include file not found! ${cleanfiles}. Quitting!"
        push_message ${status}
        echo $status 2
        exit 0
    else
    ### Read from file (cleanfiles) and delete files
    if [ $deleteit -eq 1 ]; then
        cat "$cleanfiles" | while read line; do
            #=$(find "${startdir}" -type f -name "${line}" | sed -r 's|/[^/]+$||' |sort |uniq |wc -l)
            echo "Line ${line}"
            find ${startdir} -type f -name "$line" -exec rm -f {} \;
            ((deleted_file_count++))
            pushit=1
        done
        echo $status 3
    fi
fi

if [[ ${empty_folder_count} -le 0 ]] ; then
        status="$status Found ${empty_folder_count} folders, quitting!"
        # status=""
        # push $status; status=""
        echo $status 4
        # exit 0
    else
        status="${status} And (${empty_folder_count}) empty folders found, "
        echo $status 5
        if [ $deleteit -eq 1 ]; then
                find ${startdir} -depth -exec rmdir {} \; 
                #${empty_folder_count} < 1 && status="${status} and it's been deleted!\n" || status="${status} and those were deleted!\n"
            else
                status="${status} Check set to (${deleteit}), all files/folders are safe."
        fi
fi
###     Count elapsed time
#
secs_to_human "$(($(date +%s) - ${start}))"

###     Send pushover notification
#
if  [ $pushit -eq 1 ]; then
    if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
            status="${counter} files deleted. ${status}."
            [[ $pushit ]] && push_message || echo "${status}"
        else
            status="Network is down. Can't Push message! ${elapse_status}"
    fi
else
    status="Not sending pushover! ${elapse_status}"
fi

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug stuff - Just prints whats supposed to happen
#
echo ${deleted_file_count}

if [ $debug -eq 1 ]; then
    #clear 
    echo -e "\n${ORANGE}${pushtitle}${NC}\nprinting some stuff for checking!"
    echo -e "\nNo of empty folders: ${ORANGE} ${empty_folder_count} ${NC}"
    echo -e "\nDelete filematch:  ${text_color} ${del_status^^} ${NC}"
    #echo -e "Folders:  ${ORANGE} $empty_folders_name ${NC}"
    echo -e "\nDate:               $now"
    echo -e "email	            $EMAIL_P"
    echo -e "Startdir:          ${ORANGE} ${startdir} ${NC}"
    echo "Basedir:            ${basedir}"
    echo "Scriptpath:	    $SCRIPTPATH"
    #echo -e "Debug mode:	   ${RED} $debug ${NC}"
    echo "Node:               $node"
    echo "Cleanfile list:     $cleanfiles"
    echo "No of filetypes:    $delete_types"
    echo -e "Files deleted:      $counter"
    echo -e "Folder(s) deleted:  $empty_folder_count"
    echo -e "Subject:            $pushtitle"
    #echo -e "Status:             $status"
    printf "Networkstatus:     ${text_color} $status ${NC}"
    # echo -e "\nNo of empty folders: ${$empty_folder_count}"
    # echo -e "Folders:          ${empty_folders_name}\n  "
    echo -e "\n"
fi
exit 0
