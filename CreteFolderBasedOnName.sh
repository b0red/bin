#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

for file in *; do
    #echo $file | awk -F "-" '{print $1}'  
    
    filename="$(basename "$file" )"
    
    NewFolder="$( basename "$file" | awk -F"-" '{print $1}')"

    [ -d "$NewFolder"  ] || mkdir -p "$NewFolder" && printf "created "$NewFolder"  \n"
    mv "$filename" "$NewFolder"   

done