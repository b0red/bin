#!/usr/bin/env bash
clear
### --------------------------------------------------------------------------------
#
#   current-ip.sh - file for checking current ip, regardless of NIC
#
### --------------------------------------------------------------------------------

### Source some stuff
#
SCRIPTPATH=$(dirname "$SCRIPT")
file_cc=$SCRIPTPATH/ColorCodes.inc && test -f $file_cc && source $file_cc

###     Get active Network Interface
NIC=$(\ip route | grep default | sed -e "s/^.*dev.//" -e "s/.proto.*//")

###     Get ip-info
#IP_WEWN=`/sbin/ifconfig  | /bin/grep "inet addr" | /usr/bin/cut -d ":" -f 2 | /usr/bin/cut -d " " -f 1`

IP_WEWN=$(ifconfig $NIC | awk -F "[ ]+" '/inet/ {print $3}' | awk "NR==1{print $1}")
IP_ZEWN=`wget -q -O - http://icanhazip.com/ | tail`

###     Writing to /etc (dont really know why)
#
#echo $IP_WEWN && $IP_ZEWN > /etc/script-resultsi/ip
echo -e "color: ${ORANGE} YES ${NC}"
echo -e "\nNic: 			$NIC"
echo -e "Local IP (LAN): 	$IP_WEWN"
echo -e "External IP: 		$IP_ZEWN"; echo ""
