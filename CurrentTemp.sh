#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && debug=0 || debug=1    # check if cron or interactivly
start=$(date +%s)                                               # Start the elapsed timer
###############################################################################################
##
##		Scrip for getting current weather for the loginscreen
##		Runs from cron at boot and every hour
##		(0 * * * * * /bin...)
##
###############################################################################################
LOCATION="EUR|SE|SW015|STOCKHOLM"

function current_temp(){
	LOCATION=${LOCATION}
	TODAY=`date +"%A, %e %B %Y, %R"` 
	TEMP=`curl -s "http://rss.accuweather.com/rss/liveweather_rss.asp?metric=1&locCode=$LOCATION" | \
    sed -n '/Currently:/s/.*: \(.*\): \([0-9]*\)\([CF]\).*/\2°\3, \1/p'`
	echo $TEMP > /tmp/weather_sthlm
	echo $TODAY > /tmp/weather_date
}
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
current_temp

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
if [[ $debug -eq 1 ]]; then
    clear
    echo -e "\nTemp:      $TEMP"
    echo "Today:     $TODAY"
fi
