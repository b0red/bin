#!/bin/bash
### #################################################################################################
##
## 		CurrentTvList.sh
## 		File for generating a current list of stored tvshows on my nas
##          Filelisting gets created with default vaule of last day (1) or 
##          based on value sent with ./scriptname..sh 14
##          Values are between 1 day, 1 week, 2 weeks, or 1 month
##          
##		Created by:	Patrick Osterlund
##
### #################################################################################################
clear

### --++--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#
### Debug on/off
#
debug=1
trace_debug=0

# Settings
#
TMPFOLDER="/tmp"
DATE=$(date +"%Y-%m-%d_v%V")
HEADER="filelisting for Tv-Shows"
SNAME=$(basename "$0" .sh)
FOLDERARRAY=(/media/TV/tv1 /media/TV/tv2)

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

### include files
#
file_cc=$SCRIPTPATH/ColorCodes.inc && test -f $file_cc && source $file_cc
file_email=$SCRIPTPATH/email_variables.inc && test -f $file_email && source $file_email
spinner=$SCRIPTPATH/spinner.sh  && test -f $spinner && source $spinner          # https://github.com/tlatsas/bash-spinner

### Days to search
# 
DAILY=1                                 # 1 day
WEEKLY=7                                # values between 2 and 7
BIWEEKLY=14                             # values betwwn 8 and 14  
MONTHLY=30                              # values over 15

### --++--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

if [[ $trace_debug -eq 1 ]]; then 
    set -x
    trap read debug
fi

if [ $# -ne 1 ]
then
        echo "usage : $0 <1 - 30> (Number of days to list)"
        exit 0 
fi

###     Send notification from pushover
#
function push_message {
    curl -s -F "token=$APP_TOKEN" \
    -F "user=$USER_KEY" \
    -F "title=$pushtitle" \
    -F "message=$status" https://api.pushover.net/1/messages.json
}

###     Setting values based on inout. Range between 1 and 30 (days)
#
if [[ $1 -ge 0 && $1 -le 7 ]]; then 
    VALUE=1
    NAME="daily"
elif [[ $1 -ge 8 && $1 -le 13 ]]; then
    VALUE=7
    NAME="weekly"
elif [[ $1 -le 29 && $1 -ge 14 ]]; then
    VALUE=14
    NAME="bi-weekly"
elif [[ $1 -ge 30 ]]; then 
    VALUE=30
    NAME="monthly"
else
    VALUE=1
    NAME="daily"    
fi


###     Check for folder 
#
if [ -d "$FOLDERARRAY" ]; then
  # Control will enter here if $DIRECTORY exists.
    ###     check if folder exists
    #
    if [[ ! -f $TMPFOLDER/$DATE-$NAME.txt ]]; then
        # file doesnt exist
        status="Couldn't find $DATE-$NAME.txt."
        status_color="$RED"
        touch $TMPFOLDER/$DATE-$NAME.txt
        TITLE=$($NAME $HEADER $DATE-$NAME); echo "${TITLE^} $HEADER ($DATE)" > $TMPFOLDER/$DATE-$NAME.txt
        status="$status Created $DATE-$NAME.txt!"

        ###    Check for FOLDERS in FOLDERARRAY
        #
        for FOLDERS in "${FOLDERARRAY[@]}"
        do
            # echo "Folder: $FOLDERS"
            ### Spinner
            #
            start_spinner 'Finding files'
            find $FOLDERS -mtime -${VALUE} -type f -size +2048 -exec basename {} \;| sort |uniq  >> $TMPFOLDER/$DATE-$NAME.txt
            stop_spinner $?
            #status="Created $DATE-$NAME.txt"
            status="$status and saved it to: $TMPFOLDER"
        done
        #find /media/${FOLDERARRAY} -mtime -7 -type f -size +2048 -exec basename {} \;| sort |uniq  >> /tmp/v$DATE-weekly.txt;

    else
        # file exists
        status="$DATE-$NAME.txt  exists"
        status_color="$GREEN"
        #echo "$DATE-$NAME.txt exists!"
    fi
else
    status="Media folders not found. Exiting."
fi

### --++--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug info
#
if [ $debug -eq 1 ]; then
    clear
    echo -e "\nSince debug is turned on ${GREEN} ($debug) ${NC} I'm printing the following values:"
    echo -e "\nInput \ Value:   ${GREEN} $1 ${NC}\ ${GREEN}$VALUE  ${NC}"; 
    echo "Header:           $HEADER"
    echo "Title:            $TITLE"
    echo -e "\nDate:             $DATE"
    echo "NAME:             ${NAME^}"
    echo -e "Script:           $SNAME"
    echo -e "File created:    ${ORANGE} $TMPFOLDER/$DATE-$NAME.txt ${NC}"
    ### Loop trough array
    #
    for i in "${!FOLDERARRAY[@]}"; do 
    printf "%s\t%s\n" "Folder: $i" "  ${FOLDERARRAY[$i]}"
    done
fi
echo -e "\nStatus:          ${status_color} $status ${NC}"; echo -e "\n"
exit 0