#!/bin/bash
###############################################################################
# This file is for determining what OS you are running, and
# to use that for loading os-specific functions and alias'es.
# The script AlTernator.sh does the specifics
#
# Doing some updates for this
#
################################################################################


### Check for and use this file if it exists (it should)
#
sSCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
#FILE_1=$SCRIPTPATH/variables.inc && test -f $FILE_1 && source $FILE_1
# echo FILE_1: $FILE_1

### Include colorcodes
#
source  $SCRIPTPATH/ColorCodes.inc


# Create TMPDIR
#if [ ! -d "$TMPDIR" ]; then
#   # Will enter here if $DIRECTORY does not exists, even if it contains spaces
#   mkdir -p $TMPDIR ; # echo Tmp: $TMPDIR
#fi

# Convert to lowercase, not always working
function lowercase()
{
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

################################################################################
#
# Get System Info
#
################################################################################

function shootProfile(){
    OS=`lowercase \`uname\``
    KERNEL=`uname -r`
    MACH=`uname -m`

    if [ "${U_OS}" == "cygwin*" ]  ; then
        OS=windows
    elif [ "${U_OS}" == "darwin" ]  ; then
        OS=mac
    else
        OS=`uname`
        if [ "${U_OS}" = "SunOS" ] ; then
            OS=Solaris
            ARCH=`uname -p`
            OSSTR="${OS} ${REV}(${ARCH} `uname -v`)"
        elif [ "${OS}" = "AIX" ] ; then
            OSSTR="${OS} `oslevel` (`oslevel -r`)"
        elif [ "${OS}" = "Linux" ] ; then
            if [ -f /etc/redhat-release ] ; then
                DistroBasedOn='RedHat'
                DIST=`cat /etc/redhat-release |sed s/\ release.*//`
                PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
                REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
            elif [ -f /etc/SuSE-release ] ; then
                DistroBasedOn='SuSe'
                PSUEDONAME=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
                REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
            elif [ -f /etc/mandrake-release ] ; then
                DistroBasedOn='Mandrake'
                PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
                REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
            elif [ -f /etc/debian_version ] ; then
                DistroBasedOn='Debian'
                if [ -f /etc/lsb-release ] ; then
                    DIST=`cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }'`
                    PSUEDONAME=`cat /etc/lsb-release | grep '^DISTRIB_CODENAME' | awk -F=  '{ print $2 }'`
                    REV=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }'`
                fi
            fi
            if [ -f /etc/UnitedLinux-release ] ; then
                DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
            fi
            OS=${U_OS,,}
            DistroBasedOn=`lowercase $DistroBasedOn`
            readonly OS

            DIST=`lowercase $DIST`
            readonly DIST
            readonly readonly DistroBasedOn
            readonly PSUEDONAME
            readonly REV
            readonly KERNEL
            readonly MACH
        fi
    fi
}

# convert os to lowercase
# might need to change all lowercase places
# echo ${U_OS,,} |cut -c1-6 > $TMPDIR/enviro; # echo OS really is: ${U_OS,,}

shootProfile()
if test "$1" = "P"
then
   # echo $1
    echo "" 
    echo -e "${ORANGE}=====================================================${NC}"
    echo "OS:                   ${U_OS^}"
    echo "DISTRO:           ${DIST^}"
    echo "PSUEDONAME:       ${PSUEDONAME^}"
    echo "REV:              $REV"
    echo "DistroBasedOn:    ${DistroBasedOn^}"
    echo "KERNEL:           $KERNEL"
    echo "MACHINE:          ${MACH^}"$'\n'
    echo -e "External IP:      ${GREEN} $E_IP${NC}"
    echo -e "Local IP:      ${GREEN}$L_IP${NC}"$'\n'
    echo " "; echo -e "${ORANGE}=====================================================${NC}"
    #echo $'\n'
    if [ "${U_OS}" == "Solaris" ]  ; then
        echo -e "All: uname -X"
    else
        echo -e "All: uname -a"
    fi
    echo -e "\e[0m"$'\n'
    echo "Kernel name        (-s)  : uname -s"
    echo "Nodename           (-n)  : uname -n"
    echo "Kernel-release     (-r)  : uname -r"
    echo "Kernel-version     (-v)  : uname -v"
    echo "Processor          (-p)  : uname -p"
    echo "Hardware-platform: (-i)  : uname -i"
    if [ "${U_OS}" == "Solaris" ]  ; then
        # Just to not print uname -o
        :
        echo ""
    else
        echo "OS                 (-o)  : uname -o"
    fi
    echo $'\n'
    echo -e "${ORANGE}=====================================================${NC}"
fi