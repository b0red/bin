#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && debug=0 || debug=1    # check if cron or interactivly

#######################################################################################################
###     Settings - Do NOT change!
#
#   install: sudo apt-get install inotify-tools
#
# Check for and use this file if it exists (it should)
# SCRIPT=$(readlink -f "$1")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
pushd $(dirname "$0") > /dev/null || exit
SCRIPTPATH=$(pwd -P)
popd > /dev/null ||exit

file=$SCRIPTPATH/email_variables.inc && test -f "$file" && source "$file"       # email/pushover
file_2=$SCRIPTPATH/ColorCodes.inc && test -f "$file_2" && source "$file_2"      # pretty colors on screen

### Configuration
#
folder_to_watch="/media/Downloads/complete/movies/"   # Replace with the actual path
extensions_file="list_of_trash_files.txt"             # Replace with the path to your extensions file
log_file="~/tmp/log_movie_downloads_deletes.txt"      # Optional: Path to a log file

### Function to process files based on extensions
#
process_files() {
  echo "$(date): Checking folder '$folder_to_watch'" >> "$log_file" 2>&1

  readarray -t extensions < "$extensions_file"

  deleted_count=0  # Initialize the counter

  for file in "$folder_to_watch"/*; do
    if [[ -f "$file" ]]; then
      filename=$(basename "$file")
      extension="${filename##*.}"

      for ext in "${extensions[@]}"; do
        if [[ "$extension" == "$ext" ]]; then
          echo "$(date): Deleting '$file' (matches extension '$ext')" >> "$log_file" 2>&1
          rm "$file"
          deleted_count=$((deleted_count + 1)) # Increment the counter
          break
        fi
      done
    fi
  done

  echo "$(date): Deleted $deleted_count files." >> "$log_file" 2>&1  # Log the total count
  # You can also do something with the $deleted_count variable here, like:
  # echo "Number of files deleted: $deleted_count"
}

### Initial processing
#
process_files

### Watch the folder for changes using inotifywait
#
while true; do
  inotifywait -r -e create,modify,delete,move "$folder_to_watch" --format "%w%f" | while read event; do
    echo "$(date): Inotify event: $event" >> "$log_file" 2>&1

    sleep 1

    if [[ "$(ls "$folder_to_watch")" != "$previous_ls" ]]; then
        previous_ls=$(ls "$folder_to_watch")
        process_files
    fi
  done
done