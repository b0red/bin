#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
SCRIPTPATH=$(dirname "$SCRIPT")
pushd $(dirname "$0") > /dev/null || exit
SCRIPTPATH=$(pwd -P)
popd > /dev/null ||exit

file=$SCRIPTPATH/email_variables.inc && test -f "$file" && source "$file"       # email/pushover
file_2=$SCRIPTPATH/ColorCodes.inc && test -f "$file_2" && source "$file_2"      # pretty colors on screen
file_3=$SCRIPTPATH/spinner.sh && test -f "$file_3" && source "$file_3"          # Spinner - decoration 

StateMent="Searching for folders ${ORANGE}NOT${NC} containing any ${ORANGE}$1 files ${NC}in folder:\n$PWD"

function delete_non_matching() {
   find -maxdepth 1 -type d | while read -r D
   do
   #v=$(find "$D" -iname '*.mp3' -o -iname '*.flac' -o -iname '*.m4a');
   case "$v" in
   ""  )
      echo "$D no matches"
      # rm -fr "$D" #uncomment to use
   ;;
   esac
   done
}

if [ $# -ne 1 ]
    then
        echo "Usage : DeleteNonEmpty <${ORANGE} music|movies|epub${NC} >"
        #exit 0 
    fi
   case $1 in
         music)
               clear; echo; echo -e $StateMent 
               #start_spinner 'searching...'
               #v=$(find "$D" -iname "*.mp3" -o -iname "*.flac" -o -iname "*.ogg" -o -iname "*.wav" -o -iname "*.m4a");
                  v=$(find "$D" -iname "*.mp3" -o -iname "*.flac" -o -iname "*.ogg" -o -iname "*.wav" -o -iname "*.m4a");
                  delete_non_matching
               #stop_spinner $?
               ;;
         (movie|movies)
               clear; echo; echo -e $StateMent 
               #start_spinner 'searching...'
                  v=$(find "$D" -iname '*.mov' -o -iname '*.avi' -o -iname '*.mkv' -o -iname '*.vob' -o -iname '*.ogg' -o -iname '*.wmv' -o -iname '*m4v');
                  delete_non_matching
               #stop_spinner $?
               ;;
         (epubs|ePubs|epub)
               clear; echo; echo -e $StateMent 
               #start_spinner 'searching...'
                  v=$(find "$D" -iname "*.epub" -o -iname "*.azw" -o -iname "*.mobi" -o -iname "*.pdf");
                  delete_non_matching
               #stop_spinner $?
               ;;
         *)
               echo -e nothing choosen
               ;;
      esac
      exit

}