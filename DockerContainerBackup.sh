#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

##################################################################################################
##		Small to script for backing up Docker folder
##          This script sends the files to a specified folder on Dropbox
##
##          Requirements:
##          A Dropbox account, Dropboxuploade (https://github.com/andreafabrizi/Dropbox-Uploader)
##          git clone that to DBU in this folder
##          The script also uses zip, rar and pushover (pushover.net)
##          You alsp need a email_variables file with pushover tokens and email_adress
##
#################################################################################################
clear
###     Check for internet connection
#
wget -q --spider http://google.com && Network=1 || Network=0 2>/dev/null

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#

###	    Debug on/off
#
debug=1
trace_debug=0

### 	Backup on/off
#
backup=1

### 	upload the file on/off (if network is up)
#
upload=1

###     Send notification with pushover
#
pushit=1

### 	Delete the archive fiĺe
#
delete=0

###     Softwarecheck
#
softwarecheck=1

#
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

if [ $trace_debug -eq 1 ]; then
    set -x
    trap read debug
fi
###     ######################################################################################
#           Settings#
###     #######################################################################################

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
MAIL=$SCRIPTPATH/email_variables.inc && test -f $MAIL && source $MAIL
BACKUPNAME=docker
DATE="-`date +%Y-%m-%d`.tar.gz"
FILE=~/tmp/$USER-$BACKUPNAME-`date +%Y-%m-%d`.tar.gz
SC=SoftwareChecker.sh
SC=$SCRIPTPATH/$SC && test -f $SC && source $SC
DBU=~/dropbox_uploader.sh
NODE=$(uname -n)
db_destination=Machines/$NODE/Docker/Backups
ARCHIVE="$FILE"
BU_FOLDER=~/docker/backups/$BACKUPNAME
STARTDIR=~/docker
EXCLUDES=$SCRIPTPATH/exclude.txt

###     include files
source $SCRIPTPATH/ColorCodes.inc       # just for pretty output on screen
source $SCRIPTPATH/spinner.sh           # https://github.com/tlatsas/bash-spinner

#touch $ARCHIVE
###     #######################################################################################
#           Functions
###     #######################################################################################

##      Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$SCRIPT" \
        -F "message=$status_message" https://api.pushover.net/1/messages.json
}

### 	Check for missing software, calls function included in $SC
#
if [ $softwarecheck -eq 1 ]; then
    is_it_installed rar zip ${DBU}
fi

function ListFolders {
    for folder in */; do
        echo app: "$folder" | sed 's/\/*$//g'
        tar -czvf "${folder%/}.tar.gz" "$folder"
        sleep 2
    done
}


function StopContainers {
    for d in */; do  
        echo stopping "$d"
        docker stop "$d"
        base=$(basename "$d")
        tar czvf "${base}.tar.gz" "$d"
        sleep 2
        echo starting "$d"
        docker start "$d"
    done
}

###     Function Compress folders
#
function TarFolder {                                                # tar -cvzf ~/docker/backups/mysql_202409065.tar.gz ~/docker/mysql/
    for dir in $(find . -maxdepth 1 -type d); do 
        echo "Stopping $dir"
        docker stop "$dir"
        sleep 2
            tar -cvzf ~/docker/backups/${dir}_${DATE}.tar.gz ~/docker/${dir}/
        echo "starting $dir"
        docker start "$dir"
    done
}

function FolderCompressor {
    ### Compress selected backup folder
    if [ "$backup" -eq 1 ]; then
        if [ "$trace_debug" -ne 1 ]; then
            tar -zcvf "$ARCHIVE" "$BU_FOLDER" --exclude=exclude.txt && status_file="Files compressed ok!" || status_file="Files not compressed!"
        else
            tar -zczf "$ARCHIVE" "$BU_FOLDER" --exclude=exclude.txt && status_file="Files compressed ok!" || status_file="Files not compressed!"
        fi

        if [ -f ~/tmp/"$ARCHIVE" ]; then
            status_file_color="${GREEN}"
            status="$status_file $ARCHIVE"
        else
            status="Files not compressed!"
            status_file_color="${ORANGE}"
        fi

        # Network ok/not ok
        if [ "$Network" -eq 1 ]; then 
            status_message="Network up and running. "
            status_color="${GREEN}"
            if [ "$upload" -eq 1 ]; then
                "$DBU" upload "$ARCHIVE" "$db_destination/$FILE"
                status_message="Sending $ARCHIVE to $db_destination "
                status_color="${GREEN}"
            else
                status_message="Files compressed but not sending them! "
                status_color="${ORANGE}"
            fi
        else
            status_message="$status_message Network not ok! "
            status_color="${RED}"
        fi
    else
        status_message="$status_message No backup done! "
        status_color="${ORANGE}"
    fi
}


### Send it by pushover
#
if [ $pushit -eq 1 ]; then
    push $status_message
fi

### Delete archive
#
if [ $delete -eq 1 ] ; then
        status_delete="Deleting file $ARCHIVE"
        status_delete_color="${GREEN}"
        rm -f $ARCHIVE
    else
        status_delete="Choice set to 0, not deleting $ARCHIVE"
        status_delete_color="${RED}"
fi

###     Run stuff
#
cd $STARTDIR
# ListFolders
TarFolder

###     Debuginfo
#
if [ $debug -eq 1 ]; then
    
    echo -e "\nMail:              $EMAIL_P"
    echo -e "Scriptname:        $SCRIPT"
    echo -e "DB Script:         $DBU"
    echo -e "Node:              $NODE"
    echo -e "\nDest:              $db_destination"
    echo -e "File:              $FILE"
    if [ $Network -eq 1 ]; then 
            echo -e "Network is up"
        else
            echo -e "Network is down"
    fi
    echo -e "\nStatus:            $status_file_color $status ${NC}"
    echo -e "status_message:   $status_color $status_message ${NC}"
    echo -e "status_file:       $status_file_color $status_file ${NC}"
    echo -e "status_delete:     $status_delete_color $status_delete ${NC}"
    echo -e "\nExcluded:    "        
    cat exclude.txt | tr -d '\r\n' | awk '{ print $1 }'
    echo -e "\nArkiv:               $ARCHIVE"
    echo -e "\nZip string:      $zipstring"
fi

#exit 0
