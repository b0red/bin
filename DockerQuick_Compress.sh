#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

DATE="`date +%Y-%m-%d`.tar.gz"
cd ~/docker

for dir in ~/docker/*;
  do 
	#docker stop "$dir"
    container=${dir}
    sleep 5;
    tar --exclude='~/docker/backups' --exclude='~/docker/.git' --exclude='~/docker/.vscode' -cvzf "${dir}"_"${DATE}" "${dir}"
    mv *.tar.gz ~/docker/backups
    sleep 5;
    #docker start "$container"
     #[ -d "$dir" ] && cd "$dir" && echo "Entering into $dir and installing packages"
  done;
