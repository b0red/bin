#!/bin/bash 
SHELL=/bin/bash 
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin 
export TERM=${TERM:-dumb} 
export DISPLAY=:0.0 NODE=$(uname -n) 

DESTINATION=Machines/$NODE/Backups/home/patrick/docker/backups 

for FILE in ~/docker/backups; 
    do 
        echo $FILE 
        echo $DESTINATION
        rclone move $FILE dropbox:$DESTINATION && rm -f $FILE
    done;

