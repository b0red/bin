#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && debug=0 || debug=1    # check if cron or interactivly
clear

###     Check if shell is interactive (run by user)
#[ -z "$PS1" ] && echo "Noop" || echo "Yes"
[ -t 1 ] && systat=1 || systat=0

#######################################################################################################
##
##	Info: Script for running a cronjob once a week to backup docker-compose.yml + .env to dropbox 
##  Defaults to ~/docker/compose (else it loops trough the ~/docker folder for all *.yml-files)
##
##  Requirements:
##      dropbox-uploader script (github)
##      dropbox account
##
#######################################################################################################
#
###     Info:
#       https://stackoverflow.com/questions/3214935/can-a-bash-script-tell-if-its-being-run-via-cron

clear

###     Settings
#
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null
source $SCRIPTPATH/ColorCodes.inc                   # Make pretty colors on stdout
send_me=$SCRIPTPATH/email_variables.inc && test -f $send_me && source $send_me


###     Change stuff between these lines
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug on/off
#
debug=1                                             # Just shows my debug info after a run
trace_debug=0                                       # Does a bash step debug when enabled at program run

### Settings
#
now=$(date '+%Y%m%d')                               # get date
node=$(uname -n)                                    # get node/machine name
local_path="/home/patrick/docker/compose"
DropboxPath="/Machines/${node^}/Backups/configs"    # Where to store it on dropbox 
basedir=~/docker/compose                            # Default folder (startfolder) DESTINATION=Machines/$NODE/Backups
startdir=${1:-$basedir}                             # Default startdir or users choice
files_array=(.env compose.yml *.yml)
Year=$(date '+%Y')
Month=$(date '+%B')

### Send pushover
#
pushit=1                                            # Sends a pushover notification
pushtitle="${0##*/} says:"                          # Title of the messages
pushtitle=$(echo "${pushtitle^}¨" | cut -f 1 -d '.')

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Dont change beyond this line
#

### Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug

    echo pushtitle: ${pushtitle}
fi

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#               FUNCTIONS
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### Send notification from pushover
#
function push_message() {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "message=$status" https://api.pushover.net/1/messages.json >/dev/null 2>&1
    }

###     Check for directory
#
function dir_check() {
    if [ ! -d ${basedir}  ]; then
        status="Couldn't find ${basedir}, quitting!"
        push_message ${status}
        exit 0
    fi
    }

function create_tar() {
    # if debug is on
    if [ $trace_debug -ne 1 ]; then
        #tar -cvf $ARCHIVE $FOLDER /dev/null --exclude=exclude.txt && status_file="Files compressed ok!" || status_file="Files not compressed!"
        #tar -cvf $ARCHIVE $FOLDER /dev/null && status_file="Files compressed ok!" || status_file="Files not compressed!"
        tar -cvf $ARCHIVE $FOLDER /dev/null && status_file="Files compressed ok!" || status_file="Files not compressed!"
        # if debug is of
        else
        tar -czf $ARCHIVE $FOLDER && status_file="Files compressed ok!" || status_file="Files not compressed!"
    fi
    }

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#               SCRIPT HERE
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
count=0 

for files in "${files_array[@]}"
do
    ~/dropbox_uploader.sh -s upload ${local_path}/${files} ${DropboxPath}/${now}_${files}
    count=$((count+1))
    FileArray+=" $files"
    #echo "${local_path}/${files} ${DropboxPath}/${now}_${files}"
    #sleep 2
done

status="${count} file(s) uploaded!"

[[ $pushit -eq 1 ]] && push_message || echo "$status"

[ $systat = 1 ] && systemstatus="Interactive mode"; COLOR=${RED} || systemstatus="Non interactive mode"; COLOR=${GREEN} 
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### Debug stuff
#
if [ $debug -eq 1 ]; then
    clear

    echo -e "\nOuput from ${ORANGE} ${0##*/}${NC}, printing some variables for checking!"
    echo -e "\nDebug mode:        ${RED} $debug ${NC}"
    echo -e "\nEmail	            $EMAIL_P"
    echo -e "Startdir:          ${ORANGE} $startdir ${NC}"
    echo "Scriptpath:	    $SCRIPTPATH"
    echo "Date:	            $now"
    echo -e "Year/Month:        ${ORANGE} $Year/$Month ${NC}"
    echo "Node:               ${node^}"
    echo "local path:         ${local_path}"
    echo "DropboxPath:        ${DropboxPath}"
    echo -e "\nFiles in array:    ${FileArray}" 
    echo -e "\nSubject:            $pushtitle"
    echo -e "Status:	           ${GREEN} $status ${NC}"
    echo -e "Running in:        ${COLOR} $systemstatus ${systat} ${NC}"
fi
exit 0
