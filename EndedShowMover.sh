#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
start=$(date +%s)                           # For the elapse timer
### ############################################################################################
##
##      Scan for ended tvshows and move them to another directory
##          It compares files from ended_shows.txt with a set directory
##          Requires two folders (from-folder and to-folder), a pushover account for messages,
##          rsync for moving the files/folders.
##          
##      To do
##      * create log file
##      * enter current date to that file
##      * push message to pushover
##      * maybe make a function of rsync
##      * delete read line/show from file
##
### ############################################################################################
clear

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
################################################################################################
#       Settings
################################################################################################

# Check for and use this file if it exists (it should)
# SCRIPT=$(readlink -f "$1")
# Absolute path this script is in, thus $HOME/bin
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null
SCRIPT=$(readlink -f "$0")

### Source some stuff
#
file_cc=$SCRIPTPATH/ColorCodes.inc && test -f $file_cc && source $file_cc
file_email=$SCRIPTPATH/email_variables.inc && test -f $file_email && source $file_email
spinner=$SCRIPTPATH/spinner.sh && test -f $spinner && source $spinner
ended_shows=$SCRIPTPATH/ended_shows.txt

### 	Debug on/off
#
debug=1                             # debug on / off
trace_debug=0                       # tracedebug on / off
moveit=1                            # Move files (mostly for test purposes)
pushit=1                            # Send pushover notifications
logs=~/moved_tv_shows_${now}.log    # Logfile
log_message="TV Shows moved"        # Log file message
active_folder=/media/TV/tv1         # From folder
olds_folder=/media/TV/tv2           # To folder
now=$(date '+%Y-%m-%d %H:%M')       # get date
shortdate=$(date +%y-%m-%d)         # another date    

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

###    Tracedebug
#
if [ $trace_debug -eq 1 ]; then
    set -x
    trap read debug
fi

###     #######################################################################################
#           Functions
###     #######################################################################################

###     Send notification from pushover
#
function push_message {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=Message from ${0##*/}" \
        -F "message=$status" https://api.pushover.net/1/messages.json
}

function create_log() {
    if [ -f ${logs} ]; then                  
        touch ${logs}
    fi
    echo "${log_message} - ${now}"  > ${logs}; echo $'\n' >> ${logs}
}

function secs_to_human() {
    if [[ -z ${1} || ${1} -lt 60 ]] ;then
        min=0 ; secs="${1}"
    else
        time_mins=$(echo "scale=2; ${1}/60" | bc)
        min=$(echo ${time_mins} | cut -d'.' -f1)
        secs="0.$(echo ${time_mins} | cut -d'.' -f2)"
        secs=$(echo ${secs}*60|bc|awk '{print int($1+0.5)}')
    fi
    status="Time Elapsed : ${min} minutes and ${secs} seconds. "
}
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

create_log

### Check for folders
if [ -d "$active_folder" -a -d "$olds_folder" ]; then 
    ### Read tvshows from file
    #
    cat ${ended_shows} | while read tvshow
    do
        ### Check if show exists on Active disk
        if [ -d "${active_folder}/${tvshow}" ]; then
            counter=0
            echo -e "Show: ${ORANGE} ${tvshow} ${NC} located on ${ORANGE} ${active_folder} ${NN} and will be moved to ${ORANGE} $olds_folder ${NC}!"
            echo "Show ${tvshow} will be moved!"
            counter=$((counter+1))
        else
            if [ -d "${olds_folder}/${tvshow}" ]; then
                echo -e "Couldn't find ${GREEN} ${tvshow} ${NC}, it might already be on ${GREEN} $olds_folder ${NC}."
                echo "${tvshow^} not found @${active_folder}"
            fi
        fi
    done
else
    status="Working folders not found! "; echo $status >> ${logs}
fi

### Check for folders
if [ -d "$active_folder" -a -d "$olds_folder" ]; then 
   
     ### Move the files
    if [[ ${moveit} -eq 1 ]]; then
        rsync -avzrh --progress --remove-source-files --files-from=${ended_shows} ${active_folder}/ ${olds_folder}/ 
        status="Show ${tvshow} moved. " 
        echo $status >> ${logs}
        ### Clean out folders after move
        find ${active_folder} -depth -type d -empty -delete
    else 
        status="${status}No files to be moved. "
    fi
    status="${status}Moved ${counter} files. "
    echo ${status} >> ${logs}
else
     echo ${status} >> ${logs}
fi

secs_to_human "$(($(date +%s) - ${start}))"

### Send it by pushover
#
if [[ $pushit -eq 1 ]]; then
    push_message "$status $counter"
fi

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug stuff
#
if [ $debug -eq 1 ]; then

    echo -e "\nMail:        $EMAIL_P"
    echo -e "\nStatus:      $status"
    echo -e "No of files:    $counter"
    echo -e "\nMove files (1/0):     $moveit"
    echo "Pushover (1/0):       $pushit"
    echo "Push message:             $status"
    echo -e "\nActive folder:       $active_folder"
    echo "Storage folder:      $olds_folder"
    echo "Time Elapsed       ${min} minutes and ${secs} seconds."
    echo -e "Logfile:             $logs"
    echo -e "\n"
    cat ${logs}
fi
exit 0
