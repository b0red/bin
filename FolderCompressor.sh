#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && debug=0 || debug=1    # check if cron or interactivly
start=$(date +%s)                                               # Start the elapsed timer
#######################################################################################################
##
##	Info: Script for  compressing folders recursively, and stashing them to dropbos when done
##
##      If no alternative is given when starting, it compresses folders under $HOME/docker, but you
##      can use ./FolderCompressor.sh /tmp/folder and it will run -
##
##  Requirements: archivers installed (zip, tar and more, usually by default in linux)
##                  dropbox_uploader (https://github.com/andreafabrizi/Dropbox-Uploader)
##                  pushover account (pushover.net)
##                  email_variables.inc containing info for pushover
##                  dropbox account
##
##          ToDo:  
##          * Create and write to a logfile
##          * stop and start containers before and after a backup   - Done
##          * add a counter, check for network before upload        - Done
##          * get error from tar creation and write to logfile      
##          * create a check 2 c if tarfile already exists
##           (tar error: https://superuser.com/questions/1024492/bash-if-tar-error)
##      Check for the N latest folders:
##          https://stackoverflow.com/questions/25785/delete-all-but-the-most-recent-x-files-in-bash
##      Errors in tar creation
##          https://stackoverflow.com/questions/20318852/tar-file-changed-as-we-read-it
##
#######################################################################################################
###     Settings
#
# Check for and use this file if it exists (it should)
# SCRIPT=$(readlink -f "$1")
# Absolute path this script is in, thus $HOME/bin
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

### Source some stuff
#
file_cc=$SCRIPTPATH/ColorCodes.inc && test -f $file_cc && source $file_cc
file_email=$SCRIPTPATH/email_variables.inc && test -f $file_email && source $file_email
spinner=$SCRIPTPATH/spinner.sh && test -f $spinner && source $spinner

##  Settings
#
datefolder=$(date +"%Y%m%d")                # Get date for foldername      
node=$(uname -n)                            # Get node name
basedir=~/docker                            # Startfolder
startdir=${1:-$basedir}                     # Default startdir or users choice
db_folder=/Machines                         # Dropbox destination
backupfolder=~/Backups/Docker               # Backup folder
currenttime=$(date +%H:%M)                  # Time // not sure why it's here
logs=${startdir^}_CompressedFolders.log     # Logfile
log_message="Docker compressed folders"     # Logfile title
db_dump_file="all_db_backups.sql"
pushtitle="Message from ${0##*/}!"   
pushtitle=$(echo "$pushtitle" | cut -f 1 -d '.') # Title of the messages

###     Change stuff between these lines
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

#debug=1                                    # Shows "debug" info after a run (only in run interacttive)
trace_debug=0                               # Does a bash step debug when enabled at program run
delete_file=1                               # delete archive dir if succes on upload
compress=1                                  # set to 0 just to test other stuff
timecheck=0                                 # use timecheck or not (run between 2 times)
pause=.5                                     # Just for debugging, change to larger valure if needed
pushit=1                                    # Sends a pushover notification
unset upload
upload=1                                    # Set to 1 by default, but it can be changed if no db upload script

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Dont change beyond this line

###     Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

###     Send notification from pushover
#
function push_message() {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "message=$status" https://api.pushover.net/1/messages.json
    }

###     Check for directory
#
function dir_check() {
    if [ ! -d ${basedir} ]; then
        status="Couldn't find ${basedir}, quitting!"
        push_message ${status}
        exit 0
    fi
}

function dir_create(){
    if [ ! -d ${backupfolder}/${datefolder} ]; then
        mkdir ${datefolder}
    fi
}
###     Database dump
#
function db_dump() {
    #docker exec mysql /usr/bin/mysqldump --all-databases -u root --password=${DB_PASSWORD} > ${backupfolder}/${datefolder}/${db_dump_file}
    docker exec mysql /usr/bin/mysqldump --all-databases -u root --password=${DB_PASSWORD} > ${startdir}/${db_dump_file}
    #cd tar -cjf ${db_dump_file}.tar.bz2 ${backupfolder}/${datefolder}/${db_dump_file}; sleep ${pause}
    #mv ${backupfolder}/${datefolder}_all_db_backups.sql ${backupfolder}
}

###     Count folders (not in use?)
#
function file_counter() {
    files_folder=$(ls ${backupfolder}/$datefolder | wc -l)
}

###     Check for network connection
#
function network_check() {
if echo -n >/dev/tcp/8.8.8.8/53; then
  status="Internet available. "; network=1; text_color="$GREEN"
else
  status="Offline. ";  network=0; text_color="$RED"
fi
}

###     Upload only during certain hours
#
function time_check() {
    if [[ "$currenttime" > "05:00"  ]] || [[ "$currenttime" < "09:30"  ]]; then
        echo 1
    else
        echo 2
    fi
}

###     Create logfile
#
function create_log() {
    if [ -f ${logs} ]; then                  
        touch ${logs}
    else
        echo "${log_message} - ${datefolder}"  > ${logs}; echo $'\n' >> ${logs}
    fi
}

###	Check if dropbox uploader is installed
#
function dropbox_upload_check() {
if command -v ~/dropbox_uploader.sh >/dev/null 2>&1; then
    upload=1; #echo $upload
    status="${status}Found uploader script.'"
else
    upload=0; #echo $upload
    status="${status}Uploader script wasn't found! "
fi
}

function secs_to_human() {
    if [[ -z ${1} || ${1} -lt 60 ]] ;then
        min=0 ; secs="${1}"
    else
        time_mins=$(echo "scale=2; ${1}/60" | bc)
        min=$(echo ${time_mins} | cut -d'.' -f1)
        secs="0.$(echo ${time_mins} | cut -d'.' -f2)"
        secs=$(echo ${secs}*60|bc|awk '{print int($1+0.5)}')
    fi
    status="Time Elapsed : ${min} minutes and ${secs} seconds. "
}

function file_sending() {
    if [ $upload -eq 1 -a $network -eq 1 ]; then
        ~/dropbox_uploader.sh mkdir /${db_folder}/${node^}/Docker/${datefolder}
        ~/dropbox_uploader.sh -p -s -h upload ${backupfolder}/${datefolder} /${db_folder}/${node^}/${datefolder}
        status="${status} ${foldertotal} files uploaded to /${db_folder}/${node^}/${datefolder}. "
        [[ $delete_file -eq 1 ]] && rm -rf ${backupfolder}/${datefolder}; status="${status} and deleted file. " || \
        status="${status} but file(s) not deleted. "    
    else
        status="${status}Files not uploaded! But I would've sent ${files_folder} files. "
        #status="Network is down! Can't send anything"
    fi
}

function clear_text() {
    [[ $upload -eq 1 ]] && upload=Active; text_color="$GREEN" || upload=inactive; text_color="$RED"
    [[ $pushit -eq 1 ]] && pushit=Active; text_color="$GREEN" || pushit=inactive; text_color="$RED"
}
###     ~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+

###     Jump to startdir and create folder
#
cd ${startdir}; mkdir -p ${backupfolder}/${datefolder}

###     Dump database
#
db_dump

if [ $compress -eq 1 ]; then
    ### Loop for compressing folders
    counter=0
    #start_spinner 'Compressing folders...'
    for dir in *
    #docker śtop ${dir} > /dev/null && echo Stopped container ${dir}
    # Old line:
    #do tar --exclude='2020*' --exclude='mysql' --ignore-failed-read -cjf "$dir".tar.bz2 "$dir"; sleep ${pause}
    do tar --exclude='mysql' --ignore-failed-read -cjf "$dir".tar.bz2 "$dir"; sleep ${pause}
        mv ${dir}.tar.bz2 ${backupfolder}/${datefolder} # move archives
        rm -f ${dir}.tar.bz2 
        echo "Compressed and moved ${dir} " >> $logs; sleep ${pause}; clear
        counter=$((counter+1))
       # docker start ${dir}
    done
    #stop_spinner $?
else
    status="${status}Nothing compressed or moved! "; echo $status >> $logs
fi

### Count folders
#
file_counter
if [ $compress -eq 1 ]; then
    status="${status}Compressed ${counter} files & folders. "
fi

### Check if we want to upload (& script exists) & and we have internet
### Deletes files if they are uploaded.
#
network_check

file_sending

secs_to_human "$(($(date +%s) - ${start}))"
#echo $status

file_counter

if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
    status="${counter} files compressed. ${status}Network is up!"
    [[ pushit ]] && push_message || echo "${status}"
else
    status="Network is down. Can't Push message!"
fi


# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug stuff
#
if [ $debug -eq 1 ]; then
    #clear
    clear_text
    echo -e "Since this is running interactivly, here's some ouput from ${ORANGE} ${0##*/} ${NC}!"
    echo -e "\nemail	            $EMAIL_P"
    echo -e "Backupfolder:	   ${ORANGE} ${datefolder} ${NC}"
    echo -e "basedir:           ${ORANGE} ${basedir} ${NC}"
    echo -e "Startdir:          ${ORANGE} ${startdir} ${NC}"    
    echo -e "Node:		   ${ORANGE} ${node^} ${NC}"
    echo -e "Dropbox:            ${db_folder}/${node^}"
    echo -e "Scriptpath:	    $SCRIPTPATH"
    echo -e "Debug mode:	   ${RED} $debug ${NC}"
    echo "Foldername:	    ${datefolder}"
    
    echo -e "upload (yes/no):   ${text_color} ${upload} ${NC}"
    echo -e "Send Pushmessage:  ${text_color} $pushit ${NC}"

    echo -e "\nFile1               $spinner"
    echo "File2:              $file_cc"
    echo "File3:              $file_email"
    echo -e "Folders to send:    ${files_folders}"
    echo -e "\nNetwork check:     ${text_color} ${network} ${NC}"
    echo "Foldercount:          $files_folders"
    #echo "Contercheck:        ${counter}"
    echo "Time for upload:    ${currenttime}"
    echo "Folders compressed:  ${counter}"
    #echo "From script:"
    echo -e "\nSubject:            ${pushtitle}"
    echo -e "Status:            ${text_color} $status ${NC}"
    echo "Time Elapsed        ${min} minutes and ${secs} seconds."
fi
exit 0
