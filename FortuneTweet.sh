#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

################################################################################################
##
##      Script for send tweets with twidge
##
##          Dependencies: (if any)
##          functioning mail and twidge (from repos in ubuntu)
##
################################################################################################
clear

#
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Settings
###     Absolute path this script is in, thus /home/user/bin
#
###     Variables/constants
#
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
TMP="/tmp/fortune"                              #t mpfile for storing tweets
MAX="280"                                       # Max length for tweets
touch $TMP

###     Include / Source files
#
source $SCRIPTPATH/email_variables.inc          # File with emailconfigs
source $SCRIPTPATH/ColorCodes.inc               # Just for prettu colors in terminal

###     Debug on/off
#
debug=1                                         # Debug info off/on
trace_debug=0                                   # Tracedebug off/on

##      Send mail
#
sendit=0                                        # Send notification  off/on
status=$(cat $TMP)                              # status

###     Notifications
#
mailit=0                                        # Send email off/on
pushit=1                                        # send pushover notification off/on
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

###    Tracedebug
#
if [ $trace_debug -eq 1 ]; then
    set -x
    trap read debug
fi

###     Functions
###     Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=Script_user_backup" \
        -F "message=$status_message" https://api.pushover.net/1/messages.json
}

###     Check if fortune is installed
#
if command -v /usr/games/fortune 2>/dev/null ; then
    appstatus=1

    ###     Bake the cookie
    #
    /usr/games/fortune -a > $TMP
    # test=$(cat $TMP);echo test: $test; exit 0
    COOKIELENGHT=$(awk '{t+=length($0)}END{print t}' $TMP)
    if [[ COOKIELENGHT -le $MAX ]]; then
        twidge update < "$TMP"
        status="Twidge done!"
        ### Send it
        if [[ $sendit -eq 1 ]]; then
            echo $status | msmtp $EMAIL_P
            status="$status and mail sent"
        fi
        #cat $TMP #echo $(TMP)
    else
        status="Fortunetweet is too long, i e over $MAX characters. Not tweeting!"
    fi
    echo "$status!"
else
    appstatus=0
    status="Fortune is not installed! Quitting"
    #echo $status
fi

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Debuginfo
#
if [ $debug -eq 1 ]; then
    if [ $appstatus -eq 1 ]; then
        #echo -e "Characters:    $VAL"
        echo -e "Cookielength:  $COOKIELENGHT"
        echo -e "\nMail:        $EMAIL_P"
        #echo "DB Script:    $DBU"
        #echo "Node:         $NODE"
        #echo -e "\nDest:        $db_destination"
        #echo "File:         $FILE"
        echo -e "\nStatus:      $status"
        #echo "status_message:       $status_message"
        #echo "status_file:          $status_file"
        #echo "status_delete:        $status_delete"
        #echo -e "\nExcluded:    $exclusions"
        #echo -e "\nArkiv:       $ARCHIVE"
        echo -e "\n "
    fi
else
    echo -e "$status"
fi

exit 0
