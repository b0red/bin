#!/usr/bin/env bash
##########################################################################################
##
##		Script to upload updating.txt & outdated to dropbox
##		This is for updating FreeBSD
##
##		Check for what os this is run under not yet implemented
##########################################################################################

### 	Settings
#
SCRIPT=$(readlink -e $BASH_SOURCE)
SCRIPTPATH=$(dirname "$SCRIPT")
EMAIL=$SCRIPTPATH/email_variables.inc && test -f $EMAIL && source $EMAIL
VARS=$SCRIPTPATH/variables.inc && test -f $VARS && source $VARS

###	 	Script to upload updating.txt & outdated to dropbox
#

### 	Variables
#
source /my_scripts/email_variables.inc
source /my_scripts/variables.inc

touch /tmp/updating_$SHORTDATE.txt
FILE1=/tmp/updating_$SHORTDATE.txt
FILE2=/tmp/outdated_$SHORTDATE.txt

### 	Get UPDATING info
#
echo $DATE $'\n'  > $FILE1
echo "" >> $FILE1
more /usr/ports/UPDATING >> $FILE1

### 	GET Outdated info
#
pkg version -v -l "<" | more > $FILE2

### 	Debug
#
#echo B_NAME1

### 	Get UPDATING info
#
echo $DATE $'\n'  > $FILE1
echo "" >> $FILE1
more /usr/ports/UPDATING >> $FILE1

### 	GET Outdated info
#
pkg version -v -l "<" | more > $FILE2

### 	Debug
#echo FILE:  $FILE
#echo FILE2: $FILE2
echo $DB_DEST/$MACHINE/$WORK
echo storagepath $STORAGEPATH
#echo basename: B_NAME1=$(basename $FILE1)
#echo basename: B_NAME2=$(basename $FILE2)


### 	Dropbox the files
#
droptobox upload $FILE1 $STORAGEPATH
droptobox upload $FILE2 $STORAGEPATH

### 	Delete tmp's
#
rm /tmp/*_$SHORTDATE.txt


#echo $FILE
#echo $FILE2
#echo $DB_DEST$MACHINE$WORK

###		Create folders
#
echo "Creating folder(s) in dropbox: " ${DB_DEST}/${OS}/
droptobox mkdir ${DB_DEST}/${OS}/${OPS}_${RELEASE}/$WORK

droptobox upload $FILE $STORAGEPATH$WORK
droptobox upload $FILE2 $STORAGEPATH$WORK
rm /tmp/*_$SHORTDATE.txt

echo "Files uploaded!"
exit 0
