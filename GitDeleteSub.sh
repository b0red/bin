#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear

#######################################################################################################
##
##	Info: Script for Propely deleting git submodules 
##
##  Requirements:
##
#######################################################################################################
clear

###     Settings
#
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null
source $SCRIPTPATH/ColorCodes.inc               # Make pretty colors on stdout
send_me=$SCRIPTPATH/email_variables.inc && test -f $send_me && source $send_me

### Settings
#
now=$(date '+%Y-%m-%d %H:%M')                       #get date
node=$(uname -n)                                    #get node name

#
###     Change stuff between these lines
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug on/off
#
debug=1                                         # Just shows my debug info after a run
trace_debug=0                                   # Does a bash step debug when enabled at program run

### Send pushover
#
pushit=0                                        # Sends a pushover notification
pushtitle="Message from script: ${0##*/}!"      # Title of the messages
pushtitle=$(echo "$title" | cut -f 1 -d '.')

### Clear status message                        # Clear status msg
#
status=""



# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Dont change beyond this line
#

### Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#               FUNCTIONS
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "message=$status" https://api.pushover.net/1/messages.json >/dev/null 2>&1
}
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#               SCRIPT HERE
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
if [ -z "$1" ]; then
    echo "Missing submodule"
    exit 1
fi

sn=$1

# verify that the folder is not deleted while there are unsaved changes
if [ -d $sn/.git ]; then
    cd $sn
    if [ "$?" -eq "0" ]; then
        status=$(git status --porcelain)
        if [ -n "${status}" ]; then
            printf '\n\n!!! CAUTION !!!\n\n';
            printf 'Submodule contains untracked/unstaged/unpushed changes:\n'
            echo ${status}
            printf "\n\n"
            read -n 1 -p 'Are you sure you want to continue? [Enter]' con;
            echo '';
            if [ "${con}" != "" ]; then
                echo "abort"
                exit 1
            fi
        fi

        read -n 1 -p 'Shall the repo be pushed to origin before deletion? [Enter]' con
        if [ "${con}" = "" ]; then
            git push
        fi

        echo ''
        cd -
    fi
fi


function remove() {
    line=$(cat ${1} | grep -n "\[submodule \"${sn}\"\]")
    line="${line:0:1}"
end="$(($line+2))"
sed -i.bak "$line,${end}d" ${1}
}


remove .gitmodules
remove ".git/config"

printf "\n\nDiff .gitmodules"
diff .gitmodules .gitmodules.bak

printf "\n\nDiff .git/config\n"
diff .git/config .git/config.bak

printf "\n\n"

# verify removed lines are ok
read -n 1 -p "Diffs of .gitmodules and .git/config ok? [Enter]" con;
if [ "$con" != "" ]; then
    echo "Aborted.."
    mv .gitmodules.bak .gitmodules
    mv .git/config.bak .git/config
    echo "Changes reverted"
    exit 1
fi


git add .gitmodules
git rm -rf .git/modules/${1}
git commit -m "remove submodule ${1}"
rm -rf ${1}
echo "Done!"

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### Debug stuff
#
if [ $debug -eq 1 ]; then
    clear
    echo -e "\nOuput from ${ORANGE} ${0##*/} ${NC}, printing some variables for checking!"
    echo -e "\nDebug mode:       ${RED} $debug ${NC}"
    echo -e "\nEmail	            $EMAIL_P"
    echo -e "Startdir:          ${ORANGE} $dir ${NC}"
    echo "Scriptpath:	    $SCRIPTPATH"
    echo "Date:	            $now"
    echo "Node:               $node"
    echo -e "\nSubject:            $pushtitle"
    echo -e "Status:	          ${GREEN} $status ${NC}"
    echo
fi
exit 0
