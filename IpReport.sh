#!/bin/bash

# Pushover credentials
TOKEN="awjjSRegcK5x9m1zrsANq7euVEyyiD"
USER="Y0PJsEZEGe4ubHnYaE37EEuNRWfkNO"

# Message to be sent
MESSAGE=$(curl 'https://api.ipify.org?format=json')

# URL for Pushover API
URL="https://api.pushover.net/1/messages.json"

# Time in seconds before it runs again
TIME=300

# Function to send the message
send_pushover_notification() {
    curl -s \
        --form-string "token=$TOKEN" \
        --form-string "user=$USER" \
        --form-string "message=$MESSAGE" \
        $URL
}

# Main loop to send a message every X minutes
while true; do
    send_pushover_notification
    echo "Notification sent. Next one in $(($TIME/60)) minutes."
    sleep $TIME 
done
