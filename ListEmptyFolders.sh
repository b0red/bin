#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && debug=0 || debug=1    # check if cron or interactivly
start=$(date +%s)

###     Settings - Do NOT change!
#
# Check for and use this file if it exists (it should)
# SCRIPT=$(readlink -f "$1")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
pushd $(dirname "$0") > /dev/null || exit
SCRIPTPATH=$(pwd -P)
popd > /dev/null ||exit

file=$SCRIPTPATH/email_variables.inc && test -f "$file" && source "$file"       # email/pushover
file_2=$SCRIPTPATH/ColorCodes.inc && test -f "$file_2" && source "$file_2"      # pretty colors on screen

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Change stuff between these lines

#debug=1                                            # Just shows my debug info after a run
trace_debug=0                                       # Does a bash step debug when enabled at program run

pushit=1                                            # Sends a pushover notification
deleteit=1                                          # If you want to delete files & folders

now=$(date '+%Y-%m-%d %H:%M')                       # get date
node=$(uname -n)                                    # get node/machine name
#basedir=/media/Downloads/complete                   # Startfolder
basedir=$(pwd)
startdir=${1:-$basedir}                             # Default startdir or users choice
logfile=~/tmp/empty_movie_folders.txt

pushtitle="Message from ${0##*/}!"                  # Title of the messages
pushtitle=$(echo "$pushtitle" | cut -f 1 -d '.')    # Title of the messages

#       Dont change beyond this line
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+        


# Create a log file if it doesn't exist
if [ ! -f $logfile ]; then
  touch $logfile
fi

# Get a list of all empty folders in the current directory
empty_folders=$(find "$basedir" -type d -empty)

# Iterate over the list of empty folders and write them to the log file
for empty_folder in $empty_folders; do
  echo "$empty_folder" >> $logfile
done

cat $logfile