#!/bin/bash
#
# Check if the directory is provided as an argument
if [ -z "$1"  ]; then
  echo "Usage: $0 <directory>"
  exit 1
fi

# Directory to search
DIR="$1"

# Find all files and determine their types
find "$DIR" -type f | while read -r file; do
    file --mime-type -b "$file"
    done | sort | uniq -c | sort -nr

