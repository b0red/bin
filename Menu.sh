#!/bin/bash

HEIGHT=20
WIDTH=60
CHOICE_HEIGHT=10
BACKTITLE="Simple Utils"
TITLE="Stuff for Maintaing stuff"
MENU="Choose one of the following options:"

OPTIONS=(1 "Restart Transmission"
2 "Chkrootkit"
3 "CleanUp downloadsdir"
4 "Compress docker folders"
5 "SystemClean - Clean up ubuntu"
6 "Update all - topgrade"
7 "Upgrade and remove - topgrade -c"
8 "Showmover - Moves inactive shows to disk 2"
9 "Usbmove - move stuff between usb and disk"
10 "")

CHOICE=$(dialog --clear \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --menu "$MENU" \
    $HEIGHT $WIDTH $CHOICE_HEIGHT \
    "${OPTIONS[@]}" \
    2>&1 >/dev/tty)

clear
case $CHOICE in
    1)
        echo "Restart transmission-vpn"
        docker restart transmission-vpn
        ;;
    3)
        echo "Clean Downloads dir"
        ~/bin/CleanupDownloadsDir.sh
        ;;
    4)
        echo "FolderCompressor"
        ~/bin/FolderCompressor.sh
        ;;
    5)
	    echo "SystemClean"
	    ~/bin/SystemClean.sh
	    ;;
    6)
        echo "Update all whith topgrade"
        topgrade
        ;;
    7)
        echo "Topgrade -c"
        topgrade -c
        ;;
    8)
        echo "ShowMover"
        ~/bin/ShowMover.sh
        ;;
    9)  
        echo "UsbClone"
        ~/bin/UsbClone.sh
        ;;
    10) 
        echo ""
        
        ;;
esac
