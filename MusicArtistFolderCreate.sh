#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear

#######################################################################################################
##
##	Info: Script for 
##
##  Requirements:
##
#######################################################################################################
clear

###     Settings
#
# Check for and use this file if it exists (it should)
# SCRIPT=$(readlink -f "$1")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null
file=$SCRIPTPATH/email_variables.inc && test -f $file && source $file
cleanfile=$SCRIPTPATH/delete_files.inc
# source $SCRIPTPATH/EmptyDirList.sh

###     Change stuff between these lines
#
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug on/off
#
debug=1                                     # Just shows my debug info after a run
trace_debug=0                               # Does a bash step debug when enabled at program run
echolines=0                                 # Shows what lines are read 

### Send pushover
#
pushit=1                                    # Sends a pushover notification
pushtitle="Message from the Cleanup script" # Title of the messages

### Clear status message                    # Clear status msg
#
status=""

### Just to make pretty colorfule text output
#
source $SCRIPTPATH/ColorCodes.inc

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#       Dont change beyond this line

### Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

### Settings
#
now=$(date '+%Y-%m-%d %H:%M')                       #get date
node=$(uname -n)                                    #get node name
emptyfolders=$(find $dir -type d -empty | wc -l)    #get no of empty folders

### Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "message=$status" https://api.pushover.net/1/messages.json
}

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#                           Program starts below
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Scan folders
cd /media/all/Music
ll:

### Check for directory
#
if [ ! -d ${dir} ]; then
    status="Couldn't find ${dir}, quitting!"
    push $status; status=""
    #  echo $status
    exit 0
else
    ### Read from file (Cleanfile) and count files
    #
    cat $cleanfile| while read line
do
    filecount=$(find ${dir} -type f -name '$line' | sed -r 's|/[^/]+$||' |sort |uniq |wc -l)
    # echo $filecount 
    if [ $filecount -ge 1 ]; then 
        status="Found $filecount files of type $line to delete!"
    else 
        status="No files to delete!"
    fi
    if [ $echolines -eq 1 ] ; then echo $status || echo ""; fi      #Debug
    if [ $deleteit -eq 1 ]; then
        # if [ $filecount -ge 1 ]; then
        find ${dir} -type f -name "$line" -exec rm -f {} \;
        # fi
    fi
done
fi

### Find empty dirs
#
if [ ! -s /tmp/dirs.txt ]; then
    dels=$(cat /tmp/dirs.txt)
fi

#echo dirs: $dels

### Check for empty folders
#
if [ $emptyfolders -le 0 ] ; then
    status="$status Found $emptyfolders folders to delete, quitting!"
    status=""
    # push $status; status=""
    # exit 0
else
    find $dir -type d -empty > /tmp/dirs.txt
    status="$status Marked (${emptyfolders}) empty folders for deletion, "
    # echo $status
    if [ $deleteit -eq 1 ]; then
        find ${dir} -depth -exec rmdir {} \; 
        status="$status and I have deleted $emptyfolders folder(s)"
    else
        status="$status Check set to ($deleteit), all files/folders are safe."
    fi
    # echo $status
    ### Send notification with pushover
    #
    if [ $pushit -eq 1 ] ; then
        push $status
    fi    
fi
status="$status Found $emptyfolders folder(s) to delete!"



# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug stuff
#
if [ $debug -eq 1 ]; then
    clearf
    echo -e "Ouput from ${ORANGE} ${0##*/} ${NC}, printing some variables for checking!"
    echo -e "\n# of empty folders:	${ORANGE} $emptyfolders ${NC}"
    echo -e "# files to delete:    ${ORANGE} $filecount ${NC}"
    echo -e "Files in dir.txt:      ${ORANGE} $dels ${NC}"
    echo -e "\nemail	            $EMAIL_P"
    echo -e "Startdir:          ${ORANGE} $dir ${NC}"
    echo "Scriptpath:	    $SCRIPTPATH"
    echo -e "Debug mode:	   ${RED} $debug ${NC}"
    echo "Date:	            $now"
    echo "Node:               $node"
    echo "Cleanfile:          $cleanfile"
    #echo "From script:"
    echo -e  "Startdir:     $startdir"
    echo -e "No of empty folders:     $emptyfolders"
    echo "Lines:         $line"
    echo -e "Dels:          $dels\n"
    echo -e "Subject:            $pushtitle"
    echo -e "status:	          ${GREEN} $status ${NC}"
fi
status=""
exit 0
