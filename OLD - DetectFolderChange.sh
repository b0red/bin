#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

###     ######################################################################################
#           Settings#
###     ######################################################################################

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
MAIL=$SCRIPTPATH/email_variables.inc && test -f $MAIL && source $MAIL
TRASH_FILES=$SCRIPTPATH/list_of_trash_files.inc
BASEDIR=/media/Downloads/complete/movies    # Startfolder
STARTDIR=${1:-$BASEDIR}                     # Default startdir or users choice

FOLDER=/media/Downloads/complete

file_removed() {
    status_message="$2 was removed from $1" &
    pushit; echo $status_message
}

file_modified() {
    TIMESTAMP=`date`
    status_message="[$TIMESTAMP]: The file $1$2 was modified" >> monitor_log
    pushit; echo $status_message
}

file_created() {
    TIMESTAMP=`date`
    status_message="[$TIMESTAMP]: The file $1$2 was created" >> monitor_log
    pushit; echo $status_message
    delete_trash
}

function delete_trash() {
  cat "$cleanfiles" | while read line; do
            find ${STARTDIR} -type f -name "$line" -exec rm -f {} \;
            filecount=$((filecount+1))
        done

}

function folder_for_change {
  inotifywait --recursive --event modify,move,create,delete $FOLDER
}

##      Send notification from pushover
#
function pushit {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$SCRIPT" \
        -F "message=$status_message" https://api.pushover.net/1/messages.json
}

inotifywait -q -m -r -e modify,delete,create $1 | while read DIRECTORY EVENT FILE; do
    case $EVENT in
        MODIFY*)
            file_modified "$STARTDIR" "$FILE"
            ;;
        CREATE*)
            file_created "$STARTDIR" "$FILE"
            ;;
        DELETE*)
            file_removed "$STARTDIR" "$FILE"
            ;;
    esac
done
