#!/bin/bash
# ----------------------------------------------------------------------------------------
#
#   Trying to determin type of OS
#
# ----------------------------------------------------------------------------------------
clear

if [ "$(uname)" == "Darwin" ]; then
    status="Mac/darwin"
    # Do something under Mac OS X platform
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    # Do something under GNU/Linux platform
    # check if native or winbuntu
    # echo -e "A first glance gives me \e[32mlinux\e[39m and it most probably is: "
    status="A first glance gives me linux and it most probably is"
    if grep -q Microsoft /proc/version; then
        status_2="Ubuntu on Windows"
    else
        status_2="Native Linux"
    fi
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    status_2="Windows (cygwin)"
    # Do something under Windows NT platform
fi
echo $status $status_2

