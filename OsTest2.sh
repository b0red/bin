#!/usr/bin/env bash

RELEASE=unknown

version=$( lsb_release -r | grep -oP "[0-9]+" | head -1 )
if lsb_release -d | grep -q "CentOS"; then
        RELEASE=centos$version
    elif lsb_release -d | grep -q "Ubuntu"; then
            RELEASE=ubuntu$version
        fi

        echo $RELEASE
