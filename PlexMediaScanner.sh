#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

################################################################################################
##
##      PLEX SCAN ONLY NEW/MODIFED FOLDERS SINCE LAST RUN
##          OS: Linux Ubuntu 16.04
##          Make script executable by chmod a+x plex-scan-new.sh
##          Add script to crontab -e ( paste the line bellow without ## )
##          */30 * * * *   /path to script/plex-scan-new.sh >/dev/null 2>&1
##          Make sure you disable all Plex automatic & scheduled library scans.
##
##  https://github.com/ajkis/scripts/blob/master/plex/plex-scan-new.sh
################################################################################################
clear
if pidof -o %PPID -x "$0"; then
    echo "$(date "+%d.%m.%Y %T") Exit, already running."
    exit 1
fi

#
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#

###     Settings

### 	Debug on/off
#
debug=1
trace_debug=1

###     Notifications
#
mailit=0
pushit=1

###     Variables/constants
#
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

###     Include / Source files
source $SCRIPTPATH/email_variables.inc
source $SCRIPTPATH/ColorCodes.inc
#source $SCRIPTPATH

MOVIELIBRARY="/media/all/movies/"
MOVIESECTION=2
TVLIBRARY="/media/all/TV-serier/"
TVSECTION=1
MUSICLIBRARY="/media/all/Music/"
MUSICSELECTION=3
LOGFILE="/home/plex/logs/plex-scan-new.log"
FOLDERLISTFILE="/home/plex/.cache/folderlistfile"
LASTRUNFILE="/home/plex/.cache/lastrunfile"

#
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#
export LD_LIBRARY_PATH=/usr/lib/plexmediaserver
export PLEX_MEDIA_SERVER_APPLICATION_SUPPORT_DIR=/var/lib/plexmediaserver/Library/Application\ Support

##t
:Functions
#

###     Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=Script_user_backup" \
        -F "message=$status_message" https://api.pushover.net/1/messages.json
}


###    Tracedebug
#
if [ $trace_debug -eq 1 ]; then
    set -x
    trap read debug
fi

###     Do stuff here
#
if [[ ! -f "$LASTRUNFILE" ]]; then
    touch $LASTRUNFILE
fi
echo "$(date "+%d.%m.%Y %T") PLEX SCAN FOR NEW/MODIFIED FILES AFTER: $(date -r $LASTRUNFILE)"

if [[ -f "$FOLDERLISTFILE" ]]; then
    echo "Removing previous folder list"
    rm $FOLDERLISTFILE
fi

start=$(date +'%s')

startmusic=$(date +'%s')
echo "Scanning for new files: $MUSICLIBRARY"
find "$MUSICLIBRARY" -mindepth 1 -type f -cnewer $LASTRUNFILE |
    while read mfile; do
        echo "$(date "+%d.%m.%Y %T") New file detected: $mfile" | tee -a "$LOGFILE"
        MFOLDER=$(dirname "${mfile}")
        echo "$MFOLDER" | tee -a "$FOLDERLISTFILE"
    done

    startmovies=$(date +'%s')
echo "Scanning for new files: $MOVIELIBRARY"
find "$MOVIELIBRARY" -mindepth 1 -type f -cnewer $LASTRUNFILE |
    
    while read mfile; do
        echo "$(date "+%d.%m.%Y %T") New file detected: $mfile" | tee -a "$LOGFILE"
        MFOLDER=$(dirname "${mfile}")
        echo "$MFOLDER" | tee -a "$FOLDERLISTFILE"
    done
    echo "$(date "+%d.%m.%Y %T") Movie files scanned in $(($(date +'%s') - $startmovies)) seconds" | tee -a "$LOGFILE"

    startseries=$(date +'%s')
    echo "Scanning for new files: $TVLIBRARY"
    find "$TVLIBRARY" -mindepth 2 -type f -cnewer $LASTRUNFILE |
        while read tvfile; do
            echo "$(date "+%d.%m.%Y %T") New file detected: $tvfile" | tee -a "$LOGFILE"
            TVFOLDER=$(dirname "${tvfile}")
            echo "$TVFOLDER" | tee -a "$FOLDERLISTFILE"
        done
        echo "$(date "+%d.%m.%Y %T") TV folders scanned in $(($(date +'%s') - $startseries)) seconds" | tee -a "$LOGFILE"

        echo "$(date "+%d.%m.%Y %T") Move & TV folders scanned in $(($(date +'%s') - $start)) seconds" | tee -a "$LOGFILE"
        echo "$(date "+%d.%m.%Y %T") Setting lastrun for next folder scans" | tee -a "$LOGFILE"
        touch $LASTRUNFILE
        echo "$(date "+%d.%m.%Y %T") Remove duplicates" | tee -a "$LOGFILE"
        sort "$FOLDERLISTFILE" | uniq | tee "$FOLDERLISTFILE"

        startplexscan=$(date +'%s')
        echo "$(date "+%d.%m.%Y %T") Plex scan started" | tee -a "$LOGFILE"
        readarray -t FOLDERS < "$FOLDERLISTFILE"
        for FOLDER in "${FOLDERS[@]}"
        do
            if [[  $FOLDER == "$MOVIELIBRARY"* ]]; then
                echo "$(date "+%d.%m.%Y %T") Plex scan movie folder:: $FOLDER" | tee -a "$LOGFILE"
                $LD_LIBRARY_PATH/Plex\ Media\ Scanner --scan --refresh --section "$MOVIESECTION" --directory "$FOLDER" | tee -a "$LOGFILE"
            elif [[  $FOLDER == "$TVLIBRARY"* ]]; then
                echo "$(date "+%d.%m.%Y %T") Plex scan TV folder: $FOLDER" | tee -a "$LOGFILE"
                $LD_LIBRARY_PATH/Plex\ Media\ Scanner --scan --refresh --section "$TVSECTION" --directory "$FOLDER" | tee -a "$LOGFILE"
            fi
        done
        echo "$(date "+%d.%m.%Y %T") Plex scan finished in $(($(date +'%s') - $startplexscan)) seconds" | tee -a "$LOGFILE"

        echo "$(date "+%d.%m.%Y %T") Scan completed in $(($(date +'%s') - $start)) seconds" | tee -a "$LOGFILE"




        push $status_message
        # ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        ###     Debuginfo
        #
        if [ $debug -eq 1 ]; then

            echo -e "\nMail:        $EMAIL_P"
            echo "DB Script:    $DBU"
            echo "Node:         $NODE"
            echo -e "\nDest:        $db_destination"
            echo "File:         $FILE"
            echo -e "\nStatus:      $status"
            echo "status_message:       $status_message"
            echo "status_file:          $status_file"
            echo "status_delete:        $status_delete"
            echo -e "\nExcluded:    $exclusions"
            echo -e "\nArkiv:       $ARCHIVE"
            echo -e "\n "
            echo "zipstring: "
        fi

        ### Send it by pushover
        #

        exit 0
