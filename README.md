# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for?##

My small scripts collection. Some are useful, some are just for testing and learning

### Quick summary ###

* Usually in the scripts them selves
* Version (Not using versioning right now)
* Learn Markdown

### How do I get set up? ###

git clone --recurse-submodules git@bitbucket.org:b0red/bin.git ~/bin; cd ~/bin

(Not necessary if using ' --recurse-submodules ')
git submodule init && git submodule update

### Configuration ###

None right now, it's just a repo with a bunch of bash scripts in it

### Dependencies: ###

Not really, but you might wanna do:
> sudo apt install zip rar msmtp tmux vim
>

- Probaly dropbox_uploader from github
 [ DB Uploader ] (https://github.com/andreafabrizi/Dropbox-Uploader)
 - msmtp zip & rar. 
 - More to come...

### How to run tests ###

Most of the scripts has a simple debug feature in them, just switch from 0 to 1

### Deployment instructions ###

### Update Submodules ### 

* git submodule update --recursive --remote 

or

* git submodule foreach git pull origin master

### Submodules used ###
* https://github.com/tlatsas/bash-spinner                   bash-spinner
* https://github.com/ZZROTDesign/docker-clean               docker-clean
* https://github.com/andreafabrizi/Dropbox-Uploader.git     dbu
* https://github.com/na--/ebook-tools.git                   ebook-tools
* https://github.com/sjl/t                                  todo
