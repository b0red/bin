#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

################################################################################################
##
##      Script description goes here. Small info about it and so on
##          
##          Dependencies: (if any)
##
##          Requirements: (if any)
##
################################################################################################
clear

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#
dbg=0
debug=${1:-$dbg}
trace_debug=0

###     Notifications
#
mailit=0
pushit=0

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

###     Include / Source files
source $SCRIPTPATH/email_variables.inc
source $SCRIPTPATH/ColorCodes.inc
#source $SCRIPTPATH


#
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

###     Functions
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=Message from ${0##*/}" \
        -F "message=$status_message" https://api.pushover.net/1/messages.json
}

###    Tracedebug
#
if [ $trace_debug -eq 1 ]; then
    set -x
    trap read debug
fi

###     Do stuff here
#
 

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Debuginfo
#
if [ $debug -eq 1 ]; then
    echo -e "Output from ${ORANGE} ${0##*/} ${NC} \n"
    echo -e "\nMail:        $EMAIL_P"
    echo "DB Script:    $DBU"
    echo "Node:         $NODE"
    echo -e "\nDest:        $db_destination"
    echo "File:         $FILE"
    echo -e "\nStatus:      $status"
    echo "status_message:       $status_message"
    echo "status_file:          $status_file"
    echo "status_delete:        $status_delete"
    echo -e "\nExcluded:    $exclusions"
    echo -e "\nArkiv:       $ARCHIVE"
    echo -e "\n "
    echo "zipstring: "
    status="Debug is on"
else
    status="Debug is off!"
fi

### Send it by pushover
#
[[ pushit ]] && push_message || echo ${status}

exit 0
