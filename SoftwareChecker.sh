#!/bin/bash
#
###############################################################################################
##
##		Small script to check if required software is instaalled
##
###############################################################################################

function ask_yes_or_no() {
    read -p "$1 ([y]es or [N]o): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac
}


function is_it_installed()
{
    i=0; n=0; progs=($@);
    for p in "${progs[@]}"; do
        if hash "$p" &>/dev/null
        then
            echo "$p is installed"
            ((c++))
        else
            echo "'$p' is not installed. Do you want to install '$p'"
            if [[ "yes" == $(ask_yes_or_no "Are you sure?") ]]
            then
                apt-get install $p
            else
                echo="Skipped installation of: '$p'"
                # exit 1
            fi
            ((n++))
        fi
    done
    printf "%d of %d programs were installed.\n"  "$i" "${#progs[@]}" 
    printf "%d of %d programs are missing\n" "$n" "${#progs[@]}"
echo prog: $progs

}
### Test - remove
## Just for checking
# is_it_installed mail unrar unzip foobar barfoo

