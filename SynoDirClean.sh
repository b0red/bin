#!/bin/bash
#
# DirClean.sh
#
# File for deleting all those @eaDirs on the system
# Patrick Osterlund
# Created: 2014-04-15
clear

### --++--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#
### Debug on/off
#
debug=0
trace_debug=0

# Settings
#
DATE=$(date +"%Y-%m-%d_v%V")
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
FOLDER=/media/all/Downloads

### include files
#
source $SCRIPTPATH/ColorCodes.inc       # just for pretty output on screen
source $SCRIPTPATH/email_variables.inc  # for all email accounts  
source $SCRIPTPATH/spinner.sh           # https://github.com/tlatsas/bash-spinner

### --++--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#

###     Check for root, exit if not
#
if ! [ $(id -u) = 0  ]; then
   echo "I am not root!"
      exit 1
  fi
echo "Delete all @eaDirs from ${GREEN} ${FOLDER} ${NC} down"

read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$  ]]
    then
    ### Spinner
    start_spinner 'Finding files in: ${FOLDER}'
    cd /media/all
    find . -name "@eaDir" -type d -print |while read FILENAME; do rm -rf "${FILENAME}"; done
    stop_spinner $?
   else
   "Ok, nothing to do, quitting!"; exit 1
fi
echo "Done cleaning!"

