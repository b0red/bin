#!/bin/bash
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=0.0
clear
### #############################################################################################################################
##
##      File for cleaning out systemjunk
##
##          Stolen from:
##          https://gist.github.com/Iman/8c4605b2b3ce8226b08a
##
### #############################################################################################################################

### Include color for nice output
#
file_cc=$scriptpath/ColorCodes.inc && test -f $file_cc && source file_cc

###		Check if run as root
#
if ! [ $(id -u) = 0 ]; then
   echo "This script must run as root"
   exit 1
else 
	### Check the Drive Space Used by Cached Files
	#
	printf "${ORANGE}Checking drive space${NC}\n"
	du -sh /var/cache/apt/archives


	#Clean all the log file
	#for logs in `find /var/log -type f`;  do > $logs; done

	printf "${ORANGE}Cleaning out log files${NC}\n"
	logs=`find /var/log -type f`
	for i in $logs
	do
	    > $i
	done

	#Getting rid of partial packages
	printf "${ORANGE}Cleaning out partial packages${NC}\n"
	apt-get clean && apt-get autoclean
	apt-get remove --purge -y software-properties-common

	#"Getting rid of no longer required packages
	printf "${ORANGE}Removing packages no longer required${NC}\n"
	apt-get autoremove -y


	#Getting rid of orphaned packages
	printf "${ORANGE}Removing orphaned packages${NC}\n"
	if command_exists deborphan 2>/dev/null ; then
		deborphan | xargs sudo apt-get -y remove --purge
	else
		printf "Command <${GREEB}deborphan${NC}> not found. You might want to install it."
	fi

	#Free up space by clean out the cached packages
	printf "${ORANGE}Cleaning out cached packages${NC}\n"
	apt-get clean

	# Remove the Trash
	printf "${ORANGE}Dumping trash${NC}\n"
	rm -rf /home/*/.local/share/Trash/*/**
	rm -rf /root/.local/share/Trash/*/**

	# Remove Man
	printf "${ORANGE}${NC}\n"
	#rm -rf /usr/share/man/?? 
	#rm -rf /usr/share/man/??_*

	#Delete all .gz and rotated file
	printf "${ORANGE}Removing all .gz and rotated logfiles${NC}\n"
	find /var/log -type f -regex ".*\.gz$" | xargs rm -Rf
	find /var/log -type f -regex ".*\.[0-9]$" | xargs rm -Rf

	#Cleaning the old kernels
	printf "${ORANGE}Cleaning out old kernels${NC}\n"
	dpkg-query -l|grep linux-im*
	#dpkg-query -l |grep linux-im*|awk '{print $2}'
	#printf "${ORANGE}${NC}\n"
	apt-get purge $(dpkg -l 'linux-*' | sed '/^ii/!d;/'"$(uname -r | sed "s/\(.*\)-\([^0-9]\+\)/\1/")"'/d;s/^[^ ]* [^ ]* \([^ ]*\).*/\1/;/[0-9]/!d' | head -n -1) --assume-yes
	apt-get install linux-headers-`uname -r|cut -d'-' -f3`-`uname -r|cut -d'-' -f4`

	#Cleaning is completed
	printf "\n\n${ORANGE}"
	echo "Cleaning is completed"
	printf "${NC}"
fi
