#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && debug=0 || debug=1    # check if cron or interactivly
start=$(date +%s)                                               # Start the elapsed timer
#######################################################################################################
##
##	Info: Script for 
##
##  Requirements:
##
#######################################################################################################
clear

###     Settings
#
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null
source $SCRIPTPATH/ColorCodes.inc               # Make pretty colors on stdout
send_me=$SCRIPTPATH/email_variables.inc && test -f $send_me && source $send_me

### Useful
#
now=$(date '+%Y-%m-%d %H:%M')                       #get date
node=$(uname -n)                                    #get node name

#
###     Change stuff between these lines
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#debug=1                                        # Shows if run interactive
trace_debug=0                                   # Does a bash step debug when enabled at program run
pushit=1                                        # Sends a pushover notification
pushtitle="Message from script: ${0##*/}!"      # Title of the messages
pushtitle=$(echo "$pushtitle" | cut -f 1 -d '.')
status=""

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Dont change beyond this line
#

### Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#               FUNCTIONS
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "message=$status" https://api.pushover.net/1/messages.json >/dev/null 2>&1
}

function secs_to_human() {
    if [[ -z ${1} || ${1} -lt 60 ]] ;then
        min=0 ; secs="${1}"
    else
        time_mins=$(echo "scale=2; ${1}/60" | bc)
        min=$(echo ${time_mins} | cut -d'.' -f1)
        secs="0.$(echo ${time_mins} | cut -d'.' -f2)"
        secs=$(echo ${secs}*60|bc|awk '{print int($1+0.5)}')
    fi
    status="Time Elapsed : ${min} minutes and ${secs} seconds. "
}

###     Check for network connection
#
function network_check() {
if echo -n >/dev/tcp/8.8.8.8/53; then
  status="Internet available. "; network=1; text_color="$GREEN"
else
  status="Offline. ";  network=0; text_color="$RED"
fi
}
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#               SCRIPT HERE
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+


network_check
###     Calculate elapsed time
#
secs_to_human "$(($(date +%s) - ${start}))"
if [ $pushit -eq 1 ]; then
    push_message ${status}
else
    status="Not sending message!"
fi

if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
    status="${status}Network is up!"
else
    status="Network is down. Can't Push message!"
fi
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### Debug stuff
#
if [ $debug -eq 1 ]; then
    clear
    echo -e "\nOuput from ${ORANGE} ${0##*/} ${NC}, printing some variables for checking!"
    echo -e "\nDebug mode:       ${RED} $debug ${NC}"
    echo -e "Email	            $EMAIL_P"
    echo "Scriptpath:	    $SCRIPTPATH"
    echo "Date:	            $now"
    echo "Node:               $node"
    echo -e "\nSubject:            $pushtitle"
    echo ""
    echo -e "Status:	          ${text_color} $status ${NC}"
    echo "Network:          $network"
    echo
fi
exit 0
