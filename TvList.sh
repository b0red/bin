#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
### #####################################################################################################################
##
## 		Creates last tv-shows new for the past  week
## 		should run every sunday (@00:00 0 0 * * 0)
##
##          Script uses either OneNote or Evernote for archiving purposes. This is changeable below.
##
##          Requirements; email, evernote or OneNote, a pushover account
##
### #####################################################################################################################


### #####################################################################################################################
##      Settings

###     Change stuff between these lines
# --+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-
### Controls
#
debug=1
trace_debug=0
deleteit=1

notify=1
evernote=0
pushover=1
onenote=0

NODE=$(uname -n)
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
STARTFOLDER=/media

### Source files
#
source $SCRIPTPATH/email_variables.inc
source $SCRIPTPATH/ColorCodes.inc
source $SCRIPTPATH/spinner.sh

### Variables
#
NOW=$(date +"#%G #%b #v%V")
FOLDERARRAY=(tv1 tv2)
WEEK=$(date +"%V")

EMAILBODY="TV-serier uppdaterade förra veckan!"
SUBJECT="Förra veckans Tv-Serier"

# Evernote settings
NOTEBOOK="$NODE"
TAGS="$NOW #TV #Tvserier"

# OneNote settings
SECTION="TV-Listings $NOW"
# Pushover
PUSHTITLE="Tv listing for v$WEEK"

# --+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-

### Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$PUSHTITLE" \
        -F "message=$status" https://api.pushover.net/1/messages.json >/dev/null 2>&1
}

### Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

### Check if weekly exists and is older than a week, then delete it
#
find /tmp/*weekly* -type f -mtime +7 -exec rm -rf *weekly* {} \;
# status="Found an old fiile and deleted it."
# echo its older


### Get the week nr
#
DATE=$(date +%V); echo $SUBJECT v$DATE $'\n'  > /tmp/v$DATE-weekly.txt

### Check for FOLDERS in FOLDERARRAY
#
for FOLDERS in "${FOLDERARRAY[@]}"
do
    #   echo /media/$SFA
    if [ ! -d $STARTFOLDER/$FOLDERS ]; then
        # echo /media/$FOLDERS; 
        status="Couldn't find $STARTFOLDER/$FOLDERS"
        exit 0
    else 
        # run included function
        start_spinner 'finding files'
        # Search for specified files 
        find $STARTFOLDER/${FOLDERARRAY} -mtime -7 -type f -size +2048 -exec basename {} \;| egrep '\.(mkv|avi|mpg|mpeg)'| \
            grep -oP '.*(?=[.])'| sort |uniq  >> /tmp/v$DATE-weekly.txt;
        stop_spinner $?
    fi
done

### Count number of lines in file
#
FILES=$(cat /tmp/v$DATE-weekly.txt |wc -l)
status="$status $FILES files."

### Set the messagevariable
#
#MESSAGE=$(perl -ne '/(.*) S..E.. (.*)SD TV.*/i;print "$1 - $2\n";' < /tmp/v$DATE-weekly.txt)
MESSAGE=$(cat < /tmp/v$DATE-weekly.txt)

### If notify is set to 1
#
if [[ $notify -eq 1 ]]; then
    if [[ $evernote -eq 1 ]] ; then
        echo "${SUBJECT}" $'\n' "$MESSAGE" | msmtp "${FILES} st nya i ${SUBJECT} ${NOTEBOOK} ${TAGS}" ${EMAIL_E} < /tmp/v$DATE-weekly.txt 
    elif [[ $onenote -eq 1 ]]; then

        echo "${SUBJECT}" $'\n' "$MESSAGE" | msmtp "${FILES}@${SECTION} st nya i ${SUBJECT}" ${EMAIL_1} < /tmp/v$DATE-weekly.txt 
    else
        push $status$MESSAGE
    fi

fi

### Delete weekly file
if [[ $deleteit -eq 1 ]]; then
    rm -f /tmp/v$DATE-weekly.txt
fi 

# --+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-

### Debug info - prints stuff
#
if [ $debug -eq 1 ]; then
    clear
    echo -e ""
    echo -e "Debug:         $debug"
    echo -e "Evernote:      $evernote"
    echo -e "Pushover:      $pushover"
    echo -e "OneNote:       $onenote"
    echo -e "Node:          $NODE"
    echo -e "Notebook:      $NOTEBOOK"
    echo -e "Now:          $NOW"
    echo -e "Date:          $DATE"
    # echo -e "SFA(org):  $FOLDERS"
    # echo -e "New:       $NEW"
    echo -e "# of files:    $FILES"
    for FOLDERS in "${FOLDERARRAY[@]}"
    do
        echo -e "Folder:       ${ORANGE} /media/$FOLDERS ${NC}"
    done
    echo -e "Tags:          $NOW #TV #Tvserier"
    echo -e "mail_p:        $EMAIL_P"
    echo -e "mail_1:        $EMAIL_1"
    echo -e "Pushover title:$PUSHTITLE"
    echo -e "Subject:       $SUBJECT"
    echo -e "Status:        $status"
    #echo -e ""
    echo -e "Message:\n\n $MESSAGE"
fi
# --+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-
exit 0
