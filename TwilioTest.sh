#!/bin/bash
####################################################################
##
##		Test for twilio
##
####################################################################

## This for getting a number
account_sid="ACa3aa50e96556f8ea0b066dec53a1c8aa"
auth_token="21be1cb1a712377bac361ea2b055de24"

available_number=`curl -X GET \
    "https://api.twilio.com/2010-04-01/Accounts/${account_sid}/AvailablePhoneNumbers/US/Local"  \
    -u "${account_sid}:${auth_token}" | \
    sed -n "/PhoneNumber/{s/.*<PhoneNumber>//;s/<\/PhoneNumber.*//;p;}"` \
    && echo $available_number 

## This for test sending
# Test  account
# available_number="+12066076983"

your_number="+46218138442" curl -X POST -F "Body=Hi there, your new phone number is working." \
    -F "From=${available_number}" -F "To={$your_number}" \
    "https://api.twilio.com/2010-04-01/Accounts/${account_sid}/Messages" \
    -u "${account_sid}:${auth_token}"
