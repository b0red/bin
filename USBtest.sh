IFS=$'\n'
for mtabline in `cat /etc/mtab`; do 
    device=`echo $mtabline | cut -f 1 -d ' '`
    udevline=`udevadm info -q path -n $device 2>&1 |grep usb` 
    if [ $? == 0 ] ; then
        devpath=`echo $mtabline | cut -f 2 -d ' '`
        echo "devpath: $devpath"
    fi
done
