#!/bin/bash
#
# File for mass unpacking of rar files in subfolders
# it unpacks, then deletes the residue and subfolder
# It checks if the rar'ed files are in subdirs or not
#
# Modified by Patrick Osterlund 20140602
# original file http://bit.ly/1mIF1Lx & http://bit.ly/2mF1Ri2
#
# Requires: unrar, mail
#clear

### Settings
#
debug=0
sendit=0


### Variables
# Check for and use this file if it exists (it should)
#
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

# Source variables
FILE1=$SCRIPTPATH/variables.inc && test -f $FILE1 && source $FILE1
FILE2=$SCRIPTPATH/email_variables.inc && test -f $FILE2 && source $FILE2


### Sets current directory
### Trying out new stuf
# path="$1"
### Check if dir variable is sent, otherwise use cwd
#
path="$1"
if [[ -z $1 ]]; then
  path="";path=`pwd`
  # echo "path: $path is empty, setting it to CWD"

fi

# Check if we're in the right dir
read -p "Are you sure this is the right dir: $path? [Y/n]" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]; then
	### Check for subdirs
	#
	subdircount=`find $path -maxdepth 1 -type d | wc -l`
	echo Dirs to process: $subdircount

  find . -iname '*.rar' | while read FILE
  do
    d=`dirname "$FILE"`
    f=`basename "$FILE"`

    # only unrar part01.rar or .rar
    echo $f | grep -q 'part[0-9]*.rar$' 2>&1 > /dev/null
    if [ "$?" == "0" ]; then
      echo $f | grep -q 'part01.rar$' 2>&1 > /dev/null
      if [ "$?" == "1" ]; then
        continue
      fi
    fi

    cd "$d"
    echo "Unrar $f"
    # unrar x -o+ "$f"
    unrar e -r -o- "$f" ;# *.rar;count=count+1
    cd "$path"
  done

    type=`find . -regex ".*\.\(mkv\|avi\)$"`;
    #   echo $type;mv $type "$path"
    rm -rf *.rar *.nfo *.sfv *.r* Sample
    cd "$path"
    # Remove empty sub folders if they exist
    if [ $subdircount -eq 2 ];then
    		empyfolders='find "$path" -type d -empty -exec rmdir {}\'
        echo "Deleting: $emptyfolders!"
      else
        echo "No empty dirs found, nothing to delete!"
   	fi
	#done
	if [ $sendit -eq 1 ]; then # All done, sending message
		echo "$count files extracted!" | mail -s "$count files extracted in folder: $path!" $EMAIL_P
	fi
fi


### Debug info
#
if [ $debug -eq 1 ]; then
	echo "Path:		$path"
	echo "Count:		$count"
	echo "Mailclient: 	$MAIL_CLIENT"
	echo "Mail to:		$EMAIL_P"
fi
