#!/bin/bash
rootdir="$PWD"
find "$rootdir" -type f -name '*.zip' | while read file;do
cd "$(dirname "$file")"
        if [ "$(dirname "$file")" = "$PWD" ];then
                mkdir -p "$(basename "$file" | cut -d. -f1)"
                cd "$(basename "$file" | cut -d. -f1)"
                unzip "$file"
        else
                echo "error"
                break
        fi
done
cd "$rootdir"

