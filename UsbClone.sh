#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
######################################################################################################
##
##      Info: script to sync transmission folders between PC and USB disk
##          right now USB is hardcoded, need to find a way to autodetect usb disk
##
######################################################################################################
###     Settings
#
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

###     include files
source $SCRIPTPATH/ColorCodes.inc                   # just for pretty output on screen
source $SCRIPTPATH/spinner.sh                       # https://github.com/tlatsas/bash-spinner

###     Change stuff between these lines
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

debug=0                                            # Just shows my debug info after a run
#trace_debug=1                                     # Does a bash step debug when enabled at program run
basedebug=0
trace_debug=${1:-$basedebug}


LocalUSB_Folder=/media/usbdrive
DockerLocation=~/docker/transmission-vpn/transmission-home
ServerSource=/media/Downloads/incomplete
ServerTarget=/media/Downlads/complete
MainTarget=/media/Downloads
#ServerTarget=/media/usbdrive
#Server_FromArray= ()
#Usb_ToArray=()
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Dont change beyond this line


if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Functions
#
function Get_Usb_Name(){
    IFS=$'\n'
    for mtabline in `cat /etc/mtab`; do 
        device=`echo $mtabline | cut -f 1 -d ' '`
        udevline=`udevadm info -q path -n $device 2>&1 |grep usb` 
        if [ $? == 0 ] ; then
            usbpath=`echo $mtabline | cut -f 2 -d ' '`
            echo "Usbpath: $usbpath"
        fi
    done
}

function Mount_Usb(){
    echo "Mounting USB-drive!"
    sudo mount /dev/$usbpath /media/usbdrive
}

function Create_Folders(){
    echo "Creating folders!"
    #rsync -av -f"+ */" -f"- *" "$ServerSource" "$ServerTarget"
    if [ ! -d ${usbpath}/torrents ]; then 
        mkdir $usbpath/torrents $usbpath/resume    
    fi
}

###     Copy files from USB to server
function Copy_from_USB(){
    echo "Syncing files from from_USB to Hard drive:"
    if [ $debug -eq 1 ]; then 
        echo -e "\nFrom: $usbpath/Downloads To: $MainTarget"
        rsync -azhv --progress --dry-run $usbpath/Downloads/complete/ $MainTarget
        echo -e "\nFrom: $usbpath/resume $DockerLocation"
        rsync -azhv --progress --dry-run $usbpath/Downloads/incomplete/ $MainTarget
        echo -e "\nFrom $usbpath/(resume|torrents) To: $DockerLocation"
        rsync -azhv  --progress --dry-run $usbpath/resume/ $DockerLocation
        rsync -azhv  --progress --dry-run $usbpath/torrents/ $DockerLocation
    fi
    rsync -azhv $usbpath/Downloads/complete/ $MainTarget
    rsync -azhv $usbpath/Downloads/incomplete/ $MainTarget
    rsync -azhv $usbpath/resume/ $DockerLocation
    rsync -azhv $usbpath/torrents/ $DockerLocation
    if [ $debug -ne 1 ]; then exit 0; fi
}

###     Copy files from Server to USB 
function Copy_From_Server(){
    echo "Syncing files from Hard drive to USB:"
    if [ $debug -eq 1 ]; then 

        echo "From: $ServerSource To: $usbpath/Downloads"
        rsync -azhv --progress --dry-run $ServerSource/ $usbpath/Downloads
        echo "From: $DockerLocation/resume To: $usbpath/resume "
        rsync -azhv --progress --dry-run $DockerLocation/resume/ $usbpath/resume 
        echo "From: $DockerLocation/torrents To: $usbpath/torrents "
        rsync -azhv --progress --dry-run $DockerLocation/torrents/ $usbpath/torrents
    else 
        rsync -azhv --progress --update --exclude=.donotdelete $ServerSource/ $usbpath/Downloads
        rsync -azhv --progress --update --exclude=.donotdelete $DockerLocation/resume/ $usbpath/resume 
        rsync -azhv --progress --update --exclude=.donotdelete $DockerLocation/torrents/ $usbpath/torrents
        if [ $debug -ne 1 ]; then exit 0; fi
    fi
}

function Usb_Format(){
    Get_Usb_Name
    echo -e "Formatting: ${ORANGE} ${usbpath}${NC}, are you sure?"
    echo -e "${RED} I'm disabled right now, for security!!! ${NC}"
    #sudo mkfs.vfat -F 32 -n 'USBDRIVE' $usbpath
    if [ $debug -ne 1 ]; then exit 0; fi
}

function usb_connected_check(){
    usb_exist=$(ls -l /dev/disk/by-path/*usb*  | grep -v "part" | awk '{print $NF}'|  awk -F "/" '{print $NF}' | sort)
    #[[ -z "$usb_exist" ]] && echo -e "No drive connected!\nExiting!"; exit 0 || echo "Drive is connected!"
    for usb in $(ls -l /dev/disk/by-path/*usb*  | grep -v "part" | awk '{print $NF}'|  awk -F "/" '{print $NF}')
    do
        lsblk  -n -d -o NAME,MODEL,VENDOR,SIZE,RM /dev/$usb
    done
    echo "Done!: $?"
}
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+



###     Create Menu
#
###     Just for debug, if set to 1 it wont run the menu. But with trace_debug=1 or add 1 when run
###     t will show this menu
if [ $debug -eq 0 ]; then
    HEIGHT=14
    WIDTH=54
    CHOICE_HEIGHT=6
    BACKTITLE="Copy files between USB / Hard drive - easier to copy files when on a faster net (transmssion-vpn)"
    TITLE="Rsync files between usbstick/server"
    MENU="Choose one of the following options:"

    OPTIONS=(1 "Sync files from Hard drive to USBdrive"
        2 "Sync files from USBdrive to Hard drive"
        3 "Get usb name and path"
        4 "Format USBDrive"
        5 "Check for drive"
        6 "Exit")

    CHOICE=$(dialog --clear \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --menu "$MENU" \
    $HEIGHT $WIDTH $CHOICE_HEIGHT \
    "${OPTIONS[@]}" \
    2>&1 >/dev/tty)

    clear
    case $CHOICE in
        1)
            echo "Sync files from Hard drive to USBd"
            Get_Usb_Name
            Create_Folders
            Copy_From_Server
            ;;
        2)
            echo "Sync files from USB to Hard drive"
            Get_Usb_Name
            Copy_from_USB
            ;;
        3)
            echo "Get_Usb_Name"
            Get_Usb_Name
            ;;
        4)
            echo "Formatting USB. Use with caution!"
            Usb_Format
            ;;
        5)
            echo "Check for USB"
            usb_connected_check
            ;;
        6)
            echo "Exit and quit!"
            #echo "Debug: $trace_debug"
            exit 0
            ;;
    esac
fi

####    Debug
if [ $debug -eq 1 ]; then 
    Get_Usb_Name

    echo -e "I would've copied from: ${ORANGE}(function Copy_From_Server)${NC}\n"
    Copy_From_Server
    sleep 1

    echo -e "I would've copied from: ${ORANGE}(function Copy_from_USB)${NC}\n"
    Copy_from_USB
    sleep 1

    echo -e "\nD e b u g i n f o :\n\n------------------------------------------------------------------------------"
    echo "From usbdrive:        $LocalUSB_Folder"
    echo "To usbdrive:          $usbpath"
    echo "Docker location:      $DockerLocation"
    echo "ServerSource:         $ServerSource"
    echo "Server target:        $ServerTarget"
    echo "MainTarget:           $MainTarget"
    echo "USBLocation:          $usbpath" 
    echo -e "\nChecking for mounted USB: "
    usb_connected_check 
fi
