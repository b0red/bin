#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

##################################################################################################
##
##		Small to script for backing up $USER folder
##          This script sends the files to a specified folder on Dropbox
##
##          Requirements:
##          A Dropbox account, Dropboxuploade (https://github.com/andreafabrizi/Dropbox-Uploader)
##          git clone that to DBU in this folder
##          The script also uses zip, rar and pushover (pushover.net)
##          You alsp need a email_variables file with pushover tokens and email_adress
##
#################################################################################################
clear

#
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### 	Debug on/off
#
debug=1
trace_debug=0

### 	Backup on/off
#
backup=1

### 	upload the file on/off
#
upload=0

### 	Delete the archivefiĺe
#
delete=1

### 	Files/folders to exclude
#
exclusions=(~/.git/* ~/Dropbox-Uploader/* ~/tmp/* ~/.cache/* ~/.bash_* ~/dotfiles/* ~/.tmux/* ~/.vim*)
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#
###     Settings#
#
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
MAIL=$SCRIPTPATH/email_variables.inc && test -f $MAIL && source $MAIL
FILE="$USER-`date +%Y-%m-%d`.zip"
SC=SoftwareChecker.sh
SC=$SCRIPTPATH/$SC && test -f $SC && source $SC
DBU=DBU/dropbox_uploader.sh
NODE=$(uname -n)
db_destination=Machines/$NODE/Backups
ARCHIVE="/tmp/$FILE"

if [ $trace_debug -eq 1 ]; then
    set -x
    trap read debug
fi

###     Functions 
###     Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=Script_user_backup" \
        -F "message=$status_message" https://api.pushover.net/1/messages.json
}

### 	Check for missing software
#  		calls function included in $SC
#
is_it_installed rar zip $DBU

### 	Compress userfolder
#
if [ $backup -eq 1 ]; then
    zip -r -q $ARCHIVE ~/ -x $exclusions && status_file="Files compressed ok!" || status_file="Files not compressed!"
    if [ -f $ARCHIVE ]; then
        echo $status_file $ARCHIVE
    else
        echo $status_file
        #status="File not compressed!"
    fi
    if [ $upload -eq 1 ]; then
        $DBU upload $ARCHIVE $db_destination/$FILE
        status_message="Sending $ARCHIVE to $db_destination"
    else
        status_message="Files compressed but not sending them!"
    fi
else
    status_message="No backup done!"
fi

### Send it by pushover
#
push $status_message

### Delete archive
#
if [ $delete -eq 1 ] ; then
    status_delete="Deleting file $ARCHIVE"
    rm -f $ARCHIVE
else
    status_delete="Choice set to 0, not deleting $ARCHIVE"
fi

###     Debuginfo
#
if [ $debug -eq 1 ]; then

    echo -e "\nMail:        $EMAIL_P"
    echo "DB Script:    $DBU"
    echo "Node:         $NODE"
    echo -e "\nDest:        $db_destination"
    echo "File:         $FILE"
    echo -e "\nStatus:      $status"
    echo "status_message:       $status_message"
    echo "status_file:          $status_file"
    echo "status_delete:        $status_delete"
    echo -e "\nExcluded:    $exclusions"
    echo -e "\nArkiv:       $ARCHIVE"
    echo -e "\n "
    echo "zipstring: "
fi

exit 0
