#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && trace_debug=0 || trace_debug=1    # check if cron or interactivly

### BEGIN INIT INFO
# Provides:          mail on boot
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: bootmail
# Description:       Sends email on boot
### END INIT INFO
##########################################################
##                                                      ##
##              sends mail on Boot   	                ##
##                                                      ##
##    Should live in /etc/init.d/ on Ububtu 	        ##
##########################################################

# if [ -z "$PS1" ]; then
#         #echo This shell is not interactive
#         sleep 60
# else
#         #echo This shell is interactive
#         sleep 1
# fi
sleep 60

SCRIPTPATH=$(dirname "$SCRIPT")
pushd $(dirname $0) > /dev/null || exit
SCRIPTPATH=`pwd -P`
file_cc=$SCRIPTPATH/email_variables.inc && test -f "${file_cc}" && source "${file_cc}"

###     Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

function push_message() {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "sound=tugboat" \
        -F "message=$message" https://api.pushover.net/1/messages.json
    }

### Variables
#
loc_ip=$(echo ${SSH_CONNECTION} | cut -d " " -f 1)
IP=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
ext_ip=$(wget http://ipinfo.io/ip -qO -)
node=$(uname -n)
upt=$(uptime)
who=$(who)
NOW=$(date +"%H:%M")
fortune=$(/usr/games/fortune)

### Message
#
message="Server rebooted!
Connected from: ${loc_ip} @${IP}
External ip: ${ext_ip} on ${node} and with ${upt}
${fortune}"

pushtitle="Server rebooted @${NOW}!"
push_message

exit 0
