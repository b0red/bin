#!/bin/bash
# postdownload.sh by Sander Ploegsma
{
    CP_API_KEY="8226579fbe2f499c8c68911b47b0e868"
    CP_HOST="0.0.0.0:5050"

    echo $(date +%Y-%m-%d\ %H:%M:%S) "Forcing CouchPotato rescan..."
    curl --silent -X POST "http://$CP_HOST/api/$CP_API_KEY/renamer.scan" > /dev/null
    echo $(date +%Y-%m-%d\ %H:%M:%S) "Done."
}
