#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear

#######################################################################################################
##
##	Info: Script for running a cronjob once a week to backup docker-compose.yml to dropbox 
##
##  Requirements:
##
#######################################################################################################
clear

###     Settings
#
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null
source $SCRIPTPATH/ColorCodes.inc               # Make pretty colors on stdout
send_me=$SCRIPTPATH/email_variables.inc && test -f $send_me && source $send_me


###     Change stuff between these lines
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

### Debug on/off
#
debug=1                                         # Just shows my debug info after a run
trace_debug=0                                   # Does a bash step debug when enabled at program run

### Settings
#
now=$(date '+%Y-%m-%d_%H:%M')                   #get date
node=$(uname -n)                                #get node/machine name
local_path="/home/patrick/docker/compose"
db_path="/Machines/${node^}/Docker"
files_array=(docker-compose.yml .env)

### Send pushover
#
pushit=1                                        # Sends a pushover notification
pushtitle="${0##*/} says:"                      # Title of the messages
pushtitle=$(echo "${pushtitle^}¨" | cut -f 1 -d '.')

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Dont change beyond this line
#

### Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug

   echo pushtitle: ${pushtitle}
fi

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#               FUNCTIONS
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### Send notification from pushover
#
function push_message {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "message=$status" https://api.pushover.net/1/messages.json >/dev/null 2>&1
}
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#               SCRIPT HERE
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
count=0
for files in "${files_array[@]}"
do
     ~/dropbox_uploader.sh -q upload ${local_path}/${files} ${db_path}/${now}_${files}
     count=$((count+1))
 done

 status="${count} file(s) uploaded!"

 [[ $pushit -eq 1 ]] && push_message || echo "$status"


# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### Debug stuff
#
if [ $debug -eq 1 ]; then
    clear
    echo -e "\nOuput from ${ORANGE} ${0##*/} ${NC}, printing some variables for checking!"
    echo -e "\nDebug mode:       ${RED} $debug ${NC}"
    echo -e "\nEmail	            $EMAIL_P"
    echo -e "Startdir:          ${ORANGE} $dir ${NC}"
    echo "Scriptpath:	    $SCRIPTPATH"
    echo "Date:	            $now"
    echo "Node:               $node"
    echo "local path:       ${local_path}"
    echo "DropboxPath:      ${db_path}"
    echo "FilesArray:       ${files_array}"
    echo -e "\nSubject:            $pushtitle"
    echo -e "Status:	          ${GREEN} $status ${NC}"
    echo
fi
exit 0
