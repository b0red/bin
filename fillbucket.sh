#!/bin/bash
Miakes a non git directory to a new private bitbucket repository with all files added to initial commit
# Usage = fillbucket <username> <password> <reponame>

git init
git add -A .
git commit -m "Initial commit"
curl --user $1:$2 https://api.bitbucket.org/1.0/repositories/ --data name=$3 --data is_private='true'
git remote add origin https://b0red@bitbucket.org/$1/$3.git
git push -u origin --all
git push -u origin --tags
