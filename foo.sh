#!/bin/bash

tree_contains_foo_files()
{
        # return true (0) as soon as we find a "*.foo" file
            find "$1" -type f -name "*.epub" -print0 |
                        read -r -d $'\0' file && return 0

                return 1
                
}

find . -depth -type d -print0 |
    while read -r -d $'\0' dir; do
            if ! tree_contains_foo_files "$dir"; then
                        rm -rf "$dir"
                            fi
                        done
