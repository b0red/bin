#!/bin/bash
###################################################################################
##
##		A small script for testing wich  ip adresses on your subnet
## 		are in use, they'll respond with an "is alive"
##
##		modded: 20171220
##
####################################################################################

### Debug on/off
#
debug=1

SCRIPT=$(readlink -f "$1")
SCRIPTPATH=$(dirname "$SCRIPT")
source $SCRIPTPATH/ColorCodes.inc

### Get ip to start scanning from
#
# L_IP=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}');  baseip=$(echo $L_IP | cut -d"." -f1-3)L_
LocalIP=$(hostname -I | awk '{ print $1 }')
IP=$(hostname -I | awk '{ print $1 }' | cut -d"." -f1-3 ); echo $IP; baseip=$IP

### Pingfunction
#
is_alive_ping()
{
    ping -c 1 "$1" > /dev/null
    [ $? -eq 0 ] && echo -e "Node with IP: ${ORANGE} $i ${NC} is up."
}

### Scan the range
#
for i in $baseip.{0..255}
do
    is_alive_ping $i & disown ;	dig +short -x $i
done

### Debug
#
if [ $debug -eq 1 ]; then
    echo -e "\nLocal IP:		${GREEN} $LocalIP ${NC}"
    echo -e "BaseIP:			${GREEN} $baseip.X ${NC}"
fi

exit 0
