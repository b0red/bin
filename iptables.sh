#!/bin/bash

#####################################################################
##
##		iptables-script
##			First check where iptables are, whereis iptables
##			then use that in the script (might be unnneded)
##
#####################################################################

##	location of iptables
#
IPT=/sbin/iptables

##	Variables
#
SERVER="192.168.10.60"
LAN="192.168.10.0/24"
NIC="enp0s25"
VPN="tun0"
LOOPBACK_I="lo"
SUBNET_BASE="192.168.10.0/24"
LOOPBACK="127.0.0.0/8"
GROUP=debian-transmission
ALLOW_PORT_FROM_LOCAL=9091

##	Standard
#
iptables --flush
iptables --policy INPUT DROP
iptables --policy OUTPUT ACCEPT
iptables --policy FOREWARD DROP

# Unlimited traffic on loopback, server internt
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

##	Allow responses from things we started
# stateful packet inspection
iptables -A INPUT -m --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m --state ESTABLISHED,RELATED -j ACCEPT
## might do the same thing;
#iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
#iptables -A OUTPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT

# Allow ping
iptables -A INPUT -i $NIC -p icmp --icmp-type 8 -s 0/0 -d $SERVER -j ACCEPT

## ssh Allow
#
iptables -A INPUT -p tcp -dport 2222 -j ACCEPT
iptables -A INPUT -p tcp -s $LAN -dport 2222 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#		eller
#iptables -I INPUT -i $NIC -p tcp -s 0/0 -d $SERVER -dport 2222 -j ACCEPT

##	Allow incoming to varius servers/services
#
iptables -A INPUT -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

##	Allow 443
#
iptables -A INPUT -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

##	Allow multiports
#
iptables -A INPUT -p tcp -m multiport --dports 8081,32400,5050,8181,8080, -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

## 	Drop invalid packages
iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

##	Only allow transmission (and SABnzbd) on vpn

iptables -A OUTPUT -d $LAN -p tcp --sport $ALLOW_PORT_FROM_LOCAL -m owner --gid-owner $GROUP -o $NIC -j ACCEPT
iptables -A OUTPUT -d $LAN -p udp --sport $ALLOW_PORT_FROM_LOCAL -m owner --gid-owner $GROUP -o $NIC -j ACCEPT
iptables -A OUTPUT -m owner --gid-owner $GROUP -o $VPN -j ACCEPT
iptables -A OUTPUT -m owner --gid-owner $GROUP -o lo -j ACCEPT
iptables -A OUTPUT -m owner --gid-owner $GROUP -j REJECT


###############################
#	
#	https://www.youtube.com/watch?v=XKfhOQWrUVw
##	Kolla ip ICMP requests (vad det är, funkar)
#
#	kolla vilka tjönster som körs:
#	#netstat --inet --plm
