#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear

sleep 60

#/bin/systemctl restart sendmail.service
/sbin/service sendmail restart


IP=`hostname -i` 
HOSTNAME=`hostname -f`
echo "$HOSTNAME online.  IP address: $IP" > /root/email.txt 
echo >> /root/email.txt
date >> /root/email.txt

msmtp "$HOSTNAME online" -r restart@server.domain.tld $EMAIL_P < /root/email.txt
# msmtp "$HOSTNAME online" -r restart@server.domain.tld $EMAIL_P < /root/email.txt
# msmtp "$HOSTNAME online" -r restart@server.domain.tld $EMAIL_P < /root/email.txt

#cat /root/email.txt
rm -rf /root/email.txt

#/bin/systemctl restart sendmail.service
/sbin/service sendmail restart
