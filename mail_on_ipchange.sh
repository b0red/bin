#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
[ -z "$TERM" ] || [ "$TERM" = "dumb" ] && debug=0 || debug=1    # check if cron or interactivly
start=$(date +%s)                                               # Start the elapsed timer
clear
### ##################################################################################################
##
##      Send mail on ip-change, could be run buy cron at a suitable interval
##          You can chose to send notifications by email to pushover or use the function
##      
##          Requirements:
##          Internet connection, mail (or equivalent) and account at pushover.net
##
### ##################################################################################################

#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
###     Absolute path this script is in, thus /home/user/bin
#
SCRIPTPATH=$(dirname "$SCRIPT")
SCRIPT=$(readlink -f "$0")
FILE="$SCRIPTPATH/email_variables.inc" && test -f $FILE && source $FILE
FILE_2="$SCRIPTPATH/ColorCodes.inc" && test -f $FILE_2 && source $FILE_2
DATE=$(date +%Y%m%d-%H%M%S)
NODE=$(uname -n)
DB_LOCATION="/Ḿachines/$NODE"
DB_FILE=/tmp/dropbox_ip.txt
LOGFILE=/tmp/ip_logfile.txt
FILE_ARRAY=(old_ip_internal old_ip_external current_ip_loc old_gateway_ip)

#
#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
#
trace_debug=0                # trace debug
notify=0                     # send notifications 
mailit=0                     # Send email - Needs smtp installed 
pushit=1                     # send pushover notification - req pushover account
pushtitle="IP has changed!"  # mess to send
logit=1                      # should we logit

#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
###     Info
#
# current_ip_external is the Current external IP adress
# current_ip_internal Current Internal IP adress
# old_ip_external Old external IP
# old_ip_internal Old Internal IP
# con_ip_i Current Internal IP
# connected_from SSH connection (If run interactively)

#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

###     Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

function push {
    curl -s -F "token=$APP_TOKEN" \
        -F "user=$USER_KEY" \
        -F "title=$pushtitle" \
        -F "message=$status" https://api.pushover.net/1/messages.json
}
 
###     Save ip on dropbox
#
function ip_dropbox() {
    touch $DB_FILE 
    if [ "${current_ip_external}" != $(cat /tmp/old_ip_external) ]; then 
        ~/bin/dropbox_uploader.sh upload /tmp/changed_ip.txt $DB_LOCATION
        status="IP changed. New info sent to dropbox!"
        rm -f /tmp/changed_ip.txt
    fi
}

function get_ips(){
    current_ip_external=$(wget http://ipinfo.io/ip -qO -); # echo $current_ip_external
    current_ip_internal=$(hostname -I | awk '{ print  $1 }') #echo $current_ip_internal 
    current_gateway_ip=$(curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//'); # echo $current_gateway_ip
    if [ ! -f /tmp/old_ip_external ]; then
        status="No previous external IP, writing it."
        echo $current_ip_external > /tmp/old_ip_external
    fi 
    if [ ! -f /tmp/old_ip_internal ]; then
        status="No previous internal IP, Writing it."
        echo $current_ip_internal > /tmp/old_ip_internal
    fi
        #current_ip_internal=`ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1 }'`; #  echo $loc_ip > /tmp/old_ip_internal
    if [ $debug -eq 1 ]; then connected_from=`echo $SSH_CONNECTION | cut -d " " -f 1`; fi                 
}

function write_current_ips(){
    echo $current_ip_internal > /tmp/old_ip_internal
    echo $current_ip_external > /tmp/old_ip_external
}

###     Logfile
#
function create_logfile(){
if [ ! -e "$logfile" ] ; then
    touch $logfile
fi
}

###     File array, create files needed later
#
function file_array(){
for item in ${FILE_ARRAY[*]}
do
    if [ ! -e "/tmp/$item" ]; then 
        touch "/tmp/$item"
    fi
done
}
#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
file_array
get_ips
write_current_ips

#echo c_i_e: ${current_ip_external}
#echo c_i_i ${current_ip_internal}
#echo ${old_ip_external}
#echo "cat /tmp/old_ip_internal"
#exit 0

###     Check for change on external IP
#
if [[ "${current_ip_external}" != $(cat /tmp/old_ip_external) ]]; then
    status_external="External IP has changed to: ${current_ip_external}."
    #echo ${status_external} > /tmp/changed_ip.txt
    push ${status_external}
    ip_dropbox
    [[ $debug -eq 1 ]] && status_db="Ran function 'ip_dropbox'"  
        if [[ $notify -eq 1 ]]; then 
            push ${status_external}
        fi
        echo ${current_ip_external} > /tmp/old_ip_external
    else
        status_external="No change for external IP! Not sending notifications"    
fi

###     Check for change on Internal IP (Shouldn't happen with static ip)
#
if [[ "${current_ip_internal}" != $(cat /tmp/old_ip_internal) ]]; then
    status_internal="Local IP address has changed to ${current_ip_internal}."
    #status_internal="$status"
    #echo ${status_internal} > /tmp/changed_ip.txt
    push ${status_internal}
        if [[ $notify -eq 1 ]]; then
            push "${status_internal}"
        fi
    echo ${current_ip_internal} > /tmp/old_ip_internal
else
    status_internal="No change for internal IP! Not sending notifications"
fi

### Check for change on external Router IP
#
# if [[ "${current_gateway_ip}" != $(cat /tmp/old_gateway_ip) ]]
#       then
#           status_internal="External Router IP address has changed to ${current_gateway_ip}, ($(cat /tmp/old_gateway_ip))"
#           echo $status_internal > /tmp/changed_ip.txt
#           if [[ $sendit -eq 1 ]]; then
#               echo ${NODE} ${status_internal} | msmtp "Router IP address change: " $EMAIL_P
#           fi
#           echo ${old_gateway_ip} > /tmp/old_gateway_ip
#       else
#           status_internal="No change for external Router IP! Not sending email"
# fi

### Check for router external ip
#
# Router_IP=$(curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//') | msmtp  "Router IP:: " $Router_IP $EMAIL_P

# echo $DATE >> /tmp/ip_logfile.txt

### Debug
#
if [[ $debug -eq 1 ]]; then
    clear
    echo -e "Node:                  ${ORANGE} ${NODE,,} ${NC}"
    echo -e "\nStatus external:        ${status_external}"
    echo "Status internal:        ${status_internal}"
    echo -e "\nConnected from :     $connected_from"
    echo -e "\nCurrent internal ip:   ${ORANGE} $current_ip_internal ${NC}"
    echo -e "Current external ip:   ${ORANGE} $current_ip_external ${NC}"
    echo -e "\nStatus DB:              $status_db"
    echo -e "\nold IP internal:       $(cat /tmp/old_ip_internal)"
    echo "old IP external:       $(cat /tmp/old_ip_external)"
    # echo "Old Router IP:            $(cat /tmp/old_gateway_ip)"
    # echo "Current router external IP:           $current_gateway_ip"
    echo -e "File:                  $FILE"
    echo "Email:                 $EMAIL_P"
    echo -e "Status:                 $status"
fi

exit 0

