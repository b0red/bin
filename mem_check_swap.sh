#!/bin/bash
#https://askubuntu.com/questions/157793/why-is-swap-being-used-even-though-i-have-plenty-of-free-ram

# Make sure that all text is parsed in the same language
# export LC_MESSAGES=en_US.UTF-8
# export LC_COLLATE=en_US.UTF-8
# export LANG=en_US.utf8
# export LANGUAGE=en_US:en
# export LC_CTYPE=en_US.UTF-8

#
# Calculate how much memory and swap is free
#
free_mem="$(free | grep 'Mem:' | awk '{print $7}')"
used_swap="$(free | grep 'Swap:' | awk '{print $3}')"

echo -e "Free memory:\t$free_mem kB ($((free_mem / 1024)) MiB)\nUsed swap:\t$used_swap kB ($((used_swap / 1024)) MiB)"

#
# Do the mem swap 
#
if [[ $used_swap -eq 0 ]]; then
    echo "Congratulations! No swap is in use."
elif [[ $used_swap -lt $free_mem ]]; then
    echo "Freeing swap..."
    sudo swapoff -a
    sudo swapon -a
else
    echo "Not enough free memory. Exiting."
    exit 1
fi
cls