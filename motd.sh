#!/bin/bash

# Variables (server default)
TEMP="1"  # 0 for Farenheit, 1 for Celcius
LOCCOD="27833"
# Find your own LOCCOD (location code) at the end of
# the url here: http://www.accuweather.com/rss-center.asp
# If code has a space; replace with %20

# Read and process .motd.conf file and set variables
if [ -s "$HOME/.motd.conf" ] ; then
  file=`cat "$HOME/.motd.conf" | grep -Ev '^#' | grep -Ev '^$'`
  motd=`echo "$file" | grep -Ei '^motd=.+' | sed -e 's/motd=//i' -re 's/.*(0).*/\1/'`
  if [ "$motd" == "0" ] ; then
  process="0"
  usage="0"
  weather="0"
  else
  process=`echo "$file" | grep -Ei '^process=.+'    | sed -e 's/process=//i' -re 's/.*(0).*/\1/'`
  usage=  `echo "$file" | grep -Ei '^disk usage=.+' | sed -e 's/disk usage=//i' -re 's/.*(0).*/\1/'`
  weather=`echo "$file" | grep -Ei '^weather=.+'    | sed -e 's/weather=//i' -re 's/.*(0).*/\1/'`
  TEMPu=`  echo "$file" | grep -Ei '^TEMP=.+'       | sed -e 's/TEMP=//i' -re 's/.*([01]).*/\1/'`
  LOCCODu=`echo "$file" | grep -Ei '^LOCCOD=.+'     | sed -e 's/LOCCOD=//i' -re 's/["?](.+)["?]/\1/'`
  if [ $TEMPu != "" ] ; then
    TEMP="$TEMPu"
  fi
  if [ $LOCCODu != "" ] ; then
    LOCCOD="$LOCCODu"
  fi
  fi
fi

# last login
lastlog -u $USER | tail -1 | awk '{print "Last Login...: "kr 33, kr 41, kr 49, kr 57" from "kr 24}'

# Display SSH Logins; System Uptime; System Load
if [ "$LogLoad" != "0" ] ; then
  LogLoad=`uptime`
  # Display SSH Logins
  echo $LogLoad | grep -Eo '[0-9]+ users?' \
  | awk '{print "SSH Logins...: "kr 8, kr 16" currently logged in."}'
  # Display System Uptime
  echo $LogLoad | grep -Eo 'up .+ user' \
  | sed -e 's/:/ hours /' -e 's/ min//' -re 's/^up\s+/Uptime.......: /' \
  | sed -re 's/,\s+[0-9]+ user$/ minutes/' -e 's/,//g' -e 's/00 minutes//' \
  | sed -re 's/0([1-9] minutes)/\1/' -e 's/(1 hour)s/\1/' -e 's/(1 minute)s/\1/'
  # Display System Load
  echo $LogLoad | grep -Eo 'average: .+' \
  | sed -e 's/average:/Load.........:/' -e 's/,//g' \
  | awk '{print kr 8, kr 16" (1 minute) "kr 24" (5 minutes) "kr 33" (15 minutes)"}'
fi

# Display Memory Stats
if [ "$memory" != "0" ] ; then
  memory=`free -m`
  echo "Memory MB....: `echo "$memory" | grep 'Mem: ' \
  | awk '{print "Ram used: "kr 24" free: "kr 33}'`   `echo "$memory" | grep 'Swap: ' \
  | awk '{print "Swap used: "kr 24" free: "kr 33}'`"
fi

# Display Processes
if [ "$process" != "0" ] ; then
  # (The 2 at end of the next two lines can be used to subtract out any 'offset')
  psa=$((`ps -A h | wc -l`-2))
  psu=$((`ps U $USER h | wc -l`-2))
  verb="are"
  if [ "$psu" -lt "2" ] ; then
    if [ "$psu" -eq "0" ] ; then
    psu=None
    else
    verb="is"
    fi
  fi
  echo "Processes....: $psu of the $psa running $verb yours"
fi

# Display Usage of Home Directory
if [ "$usage" != "0" ] ; then
  du -sh $HOME | awk '{print "Disk Usage...: You\47re using "kr 8"B in "kr 16}'
fi

# Display Weather
if [ "$weather" != "0" ] ; then
  weather=`curl -s http://rss.accuweather.com/rss/liveweather_rss.asp\?TEMP\=${TEMP}\&locCode\=$LOCCOD \
  | grep -E '<description>(Currently|High)' \
  | sed -e 's/.*<description>\(.*\)/\1/' -e 's/\(.*\) &lt.*/\1/' -e 's/\(&#38;#176;\)//'`
  if [ "`echo "$weather" | wc -l`" -eq "3" ] ; then
    echo "Weather......: `echo "$weather" | head -1`"
    echo "Today........: `echo "$weather" | head -2 | tail -1`"
    echo "Tomorrow.....: `echo "$weather" | tail -1`"
  fi
fi
