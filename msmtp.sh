#!/usr/bin/env bash

# Trying to automate install of smstp in freebsd
# Created by Patrick Osterlund 
# 

# Source files
source variables.inc

# Local Variables
WGDIR='/usr/local/share/certs/'
DBDIR=${DB_DEST}/${OS}/
SYSDIR='~/dotfiles/settings/'
FILE=.msmtprc

# Debug check
echo wgdir: $WGDIR; echo dbdir: $DBDIR; echo sysdir: $SYSDIR

# Check if necessary files are installed
CMDS="wget curl msmtp droptobox"

for i in $CMDS 
do
	 type -P $i &>/dev/null  && continue  || { echo "$i command not found. Please install!"; exit 1; }
done

pkg install msmtp
droptobox download $DBDIR/$FILE $SYSDIR/$FILE
ln -s $SYSDIR/$FILE ~/
wget -c -P $WGDIR http://www.geotrust.com/resources/extended-validation-ssl/certs/Equifax%20Secure%20Certificate%20Authority.crt
