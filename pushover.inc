
### Send notification from pushover
#
function push {
    curl -s -F "token=$APP_TOKEN" \
    -F "user=$USER_KEY" \
    -F "title=$pushtitle" \
    # -F "attachment=@image.jpg" \
    -F "message=$status" https://api.pushover.net/1/messages.json
}

