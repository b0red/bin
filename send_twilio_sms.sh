#!/bin/bash
# Script to send an SMS alert via Twilio.
# handy for nagios.
# supports proper Twilio API keys, so you don't have to share your user token
# https://www.twilio.com/docs/api/rest/request

function printHelpAndExit {
    echo "usage: ./`basename $0` -a \"twilio account sid\" -u \"twilio api key sid\" -p \"twilio api key secret\" -s \"sender number\" -d \"destination number\" -m \"message\"]"
    echo "optionally add -l to enable logging to syslog using the logger command"
    echo "example: ./`basename $0` -a \"ACxxxxxxxxxxxxxxxxxxxx\" -u \"SKxxxxxxxxxxxxxxxx\" -p \"xxxxxxxxxxxxxxxx\" -s \"+4401274000000\" -d \"+4401130000000\" -m \"HTTP Timed out after 10 seconds\""
    exit 1
}

LOGGER=true
#parse args
while getopts "ha:u:p:s:d:m:l" optionName; do
    case "$optionName" in
        h) printHelpAndExit;;
        a) ASID="$OPTARG";;
        u) KSID="$OPTARG";;
        p) KSECRET="$OPTARG";;
        s) SENDER="$OPTARG";;
        d) DESTINATION="$OPTARG";;
        m) MESSAGE="$OPTARG";;
        l) LOGGER=logger;;
        [?]) printHelpAndExit;;
    esac
done

BASE="https://api.twilio.com/"
API="2010-04-01/Accounts/$ASID/SMS/Messages.xml"
URL="$BASE$API"

# execute CURL call
output=$(curl -s -X POST "$URL" -d From="$SENDER" -d To="$DESTINATION" -d Body="$MESSAGE" -u $KSID:$KSECRET 2>&1)
case "$output" in
    *\<Status\>queued\</Status\>*)
        echo "queued"
        $LOGGER -t send_twilio_sms -p user.info "$output"
        exit 0
        ;;
    *)
        echo "error: ${output}"
        $LOGGER -t send_twilio_sms -p user.error                    
        ;;
esac
