#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
### ###############################################################################################
##  
##      File description
##
### ###############################################################################################
###     Settings and stuff
#   ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-
#
# dbg=1                            # Startfolder
# debug=${1:-$dbg}
delete_files=0

SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

trace_debug=0

startdir=~/tmp                            # Startfolder
workdir=${1:-$startdir}

FOLDER=~/docker/$ARCHIVE

file_cc=$SCRIPTPATH/ColorCodes.inc && test -f $file_cc && source $file_cc
file_email=$SCRIPTPATH/email_variables.inc && test -f $file_email && source $file_email
file_del=$SCRIPTPATH/delete_files.inc && test -f $file_cf && source $file_cc
#   ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
### Step debug
#
if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi
function push_message() {
    curl -s -F "token=$APP_TOKEN" \
    -F "user=$USER_KEY" \
    -F "title=Message from ${0##*/}" \
    -F "message=$status" https://api.pushover.net/1/messages.json
}

function DeleteArray() {
readarray -t deletearray < $file_del
let i=0
while (( ${#deletearray[@]} > i )); do
    if [[ $delete_files -eq 1 ]]; then
            find ${startdir} -type f -name "${deletearray[i++]}" -exec rm -f {} 2> /dev/null \; 
           else
           echo "Would've deleted all files named: ${deletearray[i++]}"
        fi
        #printf "${deletearray[i++]}"
done
}
function FolderArray() {
    for ARCHIVE in */; do  
        # echo foldername without trainling slash
        #docker stop "$d"
        echo stopping "$ARCHIVE"|sed 's/\/*$//g';
        sleep 1
        tar -cvf ~/tmp/$ARCHIVE.tar.gz $  $ARCHIVE /dev/null --exclude=exclude.txt && status_file="Folder compressed ok!" || status_file="Folder not compressed!"
        echo starting "$ARCHIVE"|sed 's/\/*$//g';
        #docker start
    done
}


FolderArray


if [ $debug -eq 1 ]; then 
    echo Debug $debug
    status="Debug is on"
else
    status="Debug is off!"
fi
[[ pushit ]] && push_message || echo ${status}

# if [ ${#@} == 0 ]; then
#     echo "Usage: $0 param1 [param2]"
#     echo "* param1: <description>"
#     echo "* param2: <description>"
# fi
