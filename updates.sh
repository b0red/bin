#!/bin/bash
#########################################################################
##
##		Script for checking for updates for motd screen
##
#########################################################################

touch /tmp/updates
sudo apt-get -V -u upgrade > /tmp/updates
last=$( tail -n 1 /tmp/updates ); echo $last > /tmp/updates
