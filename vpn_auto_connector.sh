#!/bin/bash
while true
do
    #connection="Auto Ethernet"
    connection="tun0"
    #vpn_connection="My VPN connection"
    vpn_connection="tun0"
	#run_interval="60"
	run_interval="60"

	active_connection=$(nmcli dev status | grep "${connection}")
	active_vpn=$(nmcli dev status | grep "${vpn_connection}")

	if [ "${active_connection}" -a ! "${active_vpn}" ];
	    then
		    nmcli con up id "${vpn_connection}"
		fi
    sleep $run_interval
done
