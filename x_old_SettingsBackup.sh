#!/bin/bash -p
PATH=/opt/someApp/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export DISPLAY=:0.0 ; clear
#################################################################################
##
##		Script will Backup SABnzbd, SickRage, CouchPotato
##			configs/databases as zip files to Dropbox;
##		 Files included in Backups:
#			SABnzbd - /admin/ & sabnzbd.conf;
# 			SickRage - config.ini & SickRage.db;
#			CouchPotato - config.rb & database;
#			Transmission - config;
##
#################################################################################
### 	Requirements - Dropbox uploader from github, mailutils for mail notifications;

### 	Check for and use this file if it exists (it should)
SCRIPT=$(readlink -f "$0")
### 	Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
FILE=$SCRIPTPATH/email_variables.inc && test -f $FILE && source $FILE
NODE=$(uname -n)

### 	Global settings
#
#		Dropbox Backup folder location (you need to create it manually), no "/" at the end;
dropbox=/tmp

#		Zip files password (you need to have one, at least for now)

### 	Services settings
#
# "daemon" - 1/0 - (Backup Yes/No);
# "location" - SABnzbd, SickRage, CouchPotato folder(s) (no "/" at the end);
# "filename" - Backup(s) file name(s) without extension, current date in format yyyy-mm-dd will be added to file name (example SABnzbd-16-07-22.zip);

### 	SABnzbd settings
#
sab_deamon=1
sab_location=~/.sabnzbd
sab_filename=SABnzbd

### 	SickRage settings
#
sb_deamon=1
sb_location=~/.sickrage
sb_filename=SickRage

### 	CouchPotato settings
#
cp_deamon=1
cp_location=~/.couchpotato
cp_filename=CouchPotato

### 	Transmission Settings
#
tran_deamon=1
tran_location=~/.config/transmission
tran_filename=settings.json


### 	Wanna delete Files
#
delete=1

### 	Notification settings
### 	Create log file (located in Drobpox folder)
#
log=1
log_filename=!log.txt

### 	Mail notification
#
mail_notification=1
email=$EMAIL_P
subject="Backup status"

### 	Send 2 dropbox
#
send_2_dropbox=1
script=~/Dropbox-Uploader/dropbox_uploader.sh
db_destination=Machines/$NODE


### 	Backups status
#
sab_status=""
sb_status=""
cp_status=""
tran_status=""

### 	Creating SABznbd Backup file
#
if [ $sab_deamon -eq 1 ] ; then
	zip -r -q $dropbox/$sab_filename-`date +%Y-%m-%d`.zip $sab_location/sabnzbd.conf $sab_location/admin/ $sab_location/config.ini
    	if [ -f $dropbox/$sab_filename-`date +%Y-%m-%d`.zip ]
   		then sab_status="Backup successful"
   		if [ $send_2_dropbox -eq 1 ] ; then
			$script upload $dropbox/$sab_filename-`date +%Y-%m-%d`.zip $db_destination
	   	fi
   	else sab_status="Backup failed"
    fi
    else sab_status="Backup not configured"
fi

### 	Creating SickRage Backup file
#
if [ $sb_deamon -eq 1 ] ; then
	zip -r -q $dropbox/$sb_filename-`date +%Y-%m-%d`.zip $sb_location/config.ini $sb_location/SickRage.db $sb_location/cache.db $sb_location/failed.db \
	$sb_location\sickrage.db $sb_location\cache
     	if [ -f $dropbox/$sb_filename-`date +%Y-%m-%d`.zip ]
   	then sb_status="Backup successful"
		if [ $send_2_dropbox -eq 1 ] ; then
   			$script upload $dropbox/$sb_filename-`date +%Y-%m-%d`.zip $db_destination
		fi
	else sb_status="Backup failed"
    fi
    else sb_status="Backup not configured"
fi

### Creating CouchPotato Backup file
#
if [ $cp_deamon -eq 1 ] ; then
	zip -r -q $dropbox/$cp_filename-`date +%Y-%m-%d`.zip $cp_location/config.rb $cp_location/database
    	if [ -f $dropbox/$cp_filename-`date +%Y-%m-%d`.zip ]
   		then cp_status="Backup successful"
   		if [ $send_2_dropbox -eq 1 ] ; then
			$script upload $dropbox/$cp_filename-`date +%Y-%m-%d`.zip $db_destination
   		fi
	else cp_status="Backup failed"
    fi
    else cp_status="Backup not configured"
fi

### 	Creating Transmission Backup file
#
if [ $tran_deamon -eq 1 ] ; then
	# zip -r -q $dropbox/$tran_filename-`date +%Y-%m-%d`.zip $tran_location/config.rb $tran_location/database
	zip -r -q $dropbox/$tran_filename-`date +%Y-%m-%d`.zip $tran_location/settings.json
    	if [ -f $dropbox/$tran_filename-`date +%Y-%m-%d`.zip ]
   		then cp_status="Backup successful"
   		if [ $send_2_dropbox -eq 1 ] ; then
			$script upload $dropbox/$tran_filename-`date +%Y-%m-%d`.zip $db_destination
   		fi
	else tran_status="Backup failed"
    fi
    else tran_status="Backup not configured"
fi

### 	Creating log file
#
if [ $log -eq 1 ] ; then
	echo -e "Backup status: $(date +%Y-%m-%d)\r
	SABznbd: $sab_status\r
	SickRage: $sb_status\r
	CouchPotato: $cp_status\r
	Transmission: $tran_status\r
	\n--\n$fortune\r---\r" >> $dropbox/$log_filename
fi

### 	Sending e-mail
#
if [ $mail_notification -eq 1 ] ; then
	mail -s "$subject" $email <<< "Backup status: $(date +%Y-%m-%d)
	SABnzbd: $sab_status
	SickRage: $sb_status
	CouchPotato: $cp_status
	Transmission: $tran_status
	---"
fi

### 	Clean up
#
if [ $delete -eq 1 ] ; then
	rm -rf /tmp/*.zip
fi
