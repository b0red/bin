#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0
clear
####################################################################################################
##
##		Creates a list of last tv-shows new for this week
## 		should run every sunday (crontab)
##      0 0 * * 0 root /home/$USER/bin/command
##
##      This script uses Pushover and Evernote to store information and sending notifcations
##      If you dont use them, then set evernote=0 and pushover=0
##
##
##		Changes:
##      2014-08-06:     Changed tempdir
##		2016-12-26		Fixed for it to run under ubunt
##      2018-08-31      Updating and adding checks
##      2018-09-xx      
##
#####################################################################################################
clear

### Absolute path this script is in, thus /home/user/bin
#
SCRIPTPATH=$(dirname "$SCRIPT")
EMAIL=$SCRIPTPATH/email_variables.inc && test -f $EMAIL && source $EMAIL
SCRIPT=$(readlink -f "$0")
NOW=$(date +"#%G #%b #v%V")
WEEK=$(date +%V)
#
###     Things you can change are between these too lines
#       ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###                     set to 0 if not in use
###                     set to 0 if not in use
debug=1
trace_debug=0

### Evernote on/off     set to 0 if not in use
#
evernote=0

### Pushover on/off     set to 0 if not in use
#
pushit=1                 

### Delete file after run
#
deleteit=1

### Folder for tv-shows
#
SEARCHFOLDER="/media/tv1/"

### Mailstuff
#
EMAILSUBJECT="Nya avsnitt i Tv-Serier"
EMAILBODY="Följande nya avsnitt i tv-serier v$WEEK"

###     No editing after this line
#       ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

if [ $trace_debug -eq 1 ] ; then
    set -x
    trap read debug
fi

###     Checl if doesn't folder exists
#
if [ ! -d ${dir} ]; then
    status="Couldn't find folder: ${dir}, quitting!"
    appstatus=0
    push $status
    #  echo $status
    exit 0
else

    ###     Create tmpdir if not exists
    #
    if [ ! -d /tmp ]; then
        mkdir -p /tmp
    fi

    
    #if [ ! -d ${SEARCHFOLDER} ]; then
    #    echo "Folder: ${SEARCHFOLDER} not found! Exiting"
    #    status_folder_exit="Couldn't find ${SEARCHFOLDER}, so nothing to do. Exiting."
    #    #exit 0
    #else
    #    status_folder_exit=""
    #fi


    ### Get the week nr and use it as filename
    #
    FILENAME="/tmp/v$WEEK-weekly.txt" #; echo $FILENAME

    ### Write headline to $FILENAME
    #
    echo $EMAILBODY > $FILENAME

    ### If file doesnt exist create it
    #
    if [ ! -f $FILENAME ]; then
        ### Search for new files and save result to text file
        #
        find ${SEARCHFOLDER} -mtime -7 -type f -size +2048 -printf '%f\n' | cut -d'.' --complement -f2- >> $FILENAME
        echo result: $RESULT
        ### Count number of lines in file
        #
        COUNT=$(wc -l < $FILENAME)
        STATUS="Found $NEW new files! Saving result to $FILENAME"
    else
        STATUS="File already created, No need to re-index $SEARCHFOLDER."
    fi

    ### Set the messagevariable and strip season/episode and quality
    #
    echo "Body: $BODY"
    BODY=$(cat < $FILENAME)
    echo "Body 2: $BODY"

    ### if mail exists then send stuff
    # 
    if mail -v bash >/dev/null 2>&1; then

        ### Mailto Evernote
        #
        if [ $evernote -eq 1 ]; then

            NOTEBOOK="@$(uname -n)"
            TAGS="$NOW #TV #Tvserier"

            echo "${EMAILSUBJECT}" $'\n' "$MESS" | mail -s "${NEW} avsnitt i ${EMAILSUBJECT} ${NOTEBOOK} ${TAGS}"
            mail $EMAIL_E -s "$COUNT avsnitt i $EMAILSUBJECT $NOTEBOOK $TAGS" <<< $BODY
            status_evernote="Evernote set to 1, sending info: $BODY"
        else
            status_evernote="Evernote set to 0, not sending info"
        fi

        ### Send to Pushover
        #
        if [ $pushit -eq 1 ] ; then
            mail $EMAIL_P $SUBJECT <<< $BODY
            pushit_status="Pushit set to 1, pushing info: $BODY"
        else
            pushit_status="Pushit set to 0, not pushing info"
        fi
    else
        mail_status="No mail installed on system, exiting"
    fi


    ### Delete file when done
    #
    if [ $deleteit -eq 1 ]; then
        rm $FILENAME
        status_deleteit="Delete set to 1, deleting $FILENAME"
    else
        status_deleteit="Delete set to 0, nothing to delete"
    fi
else 
    status="Folder $SEARCHFOLDER not found. quitting!"
    echo -e ${ORANGE}$status${NC}
fi

### Just for checking
#
if [ $appstatus -eq 1]; then
    if [ $debug -eq 1 ]; then
        status_debug="Debug set to 1"
        clear
        echo -e "\n"
        echo    "Scriptname:    ${0##*/}"
        echo 	"SEARCHFOLDER:	$SEARCHFOLDER"
        echo 	"NOTEBOOK:	$NOTEBOOK"
        echo 	"EMAILBODY:	$EMAILBODY"
        echo 	"EMAILSUBJECT:	$EMAILSUBJECT"
        echo -e "\nFilename:	$FILENAME"
        echo 	"Mailbody:	$BODY"
        echo -e	"\nNOW:		$NOW"
        echo -e	"TAGS:		$TAGS\n"
        echo 	"Mess:          $MESS"
        echo 	"STATUS:	        $STATUS"
        echo 	"Filename:      $FILENAME"
        echo 	"Mess:          ${EMAILSUBJECT} ${MESS} ${NEW} ${NOTEBOOK} ${TAGS} ${EMAILTO} ${EMAILSECOND}"
        echo -e "\nStatuses:"
        echo    "Debug:     $status_debug"
        echo    "Evernote:  $status_evernote"
        echo    "Pushover:  $status_pushit"
        echo    "Delete it: $status_deleteit"
        echo    "Folder:    $status_folder_exit"
    else
        status_debug="Debug set to 0"
    fi
else
    echo $status
fi

exit 0
