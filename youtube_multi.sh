#!/bin/bash -p
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export TERM=${TERM:-dumb}
export DISPLAY=:0.0

################################################################################################
##
##      Script description goes here. Small info about it and so on
##          
##          Dependencies: (if any)
##
##          Requirements: (if any)
##
################################################################################################
clear

# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#
dbg=0
debug=${1:-$dbg}
trace_debug=0
wait=1

pushit=0                                            # Sends a pushover notification
pushtitle="Message from ${0##*/}!"                  # Title of the messages
pushtitle=$(echo "$pushtitle" | cut -f 1 -d '.')    # Title of the messages
deleteit=1                                          # If you want to delete empty folders



# Check for and use this file if it exists (it should)
# SCRIPT=$(readlink -f "$1")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

###     Include / Source files
source $SCRIPTPATH/email_variables.inc
source $SCRIPTPATH/ColorCodes.inc

###     Filelist
FILELIST=$SCRIPTPATH/youtube_dl_list.txt

#
# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

###     Functions
#

#       Send notification from pushover
#
function push_message() {
    curl -s -F "token=$APP_TOKEN" \
    -F "user=$USER_KEY" \
    -F "title=$pushtitle" \
   -F "message=$status" https://api.pushover.net/1/messages.json
}

#
if [ $trace_debug -eq 1 ]; then
    set -x
    trap read debug
fi

###     Do stuff here
#

###     loop through files in download lost

for files in $FILELIST;  do echo ${files}; done;


# ~--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
###     Debuginfo
#
if [ $debug -eq 1 ]; then
    echo -e "Output from ${ORANGE} ${0##*/} ${NC} \n"
    echo -e "\nMail:        $EMAIL_P"
    echo "DB Script:    $DBU"
    echo "Node:         $NODE"
    echo -e "\nDest:        $db_destination"
    echo "Filelist:         $FILELIST"
    echo -e "\nStatus:      $status"
    echo "status_message:       $status_message"
    echo "status_file:          $status_file"
    echo "status_delete:        $status_delete"
    echo -e "\nExcluded:    $exclusions"
    echo -e "\nArkiv:       $ARCHIVE"
    echo -e "\n "
    echo "zipstring: "
    status="Debug is on"
else
    status="Debug is off!"
fi

### Send it by pushover
#
[[ pushit ]] && push_message || echo ${status}

exit 0
